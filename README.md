# Pokiro ポキろ

> Poker + Hero 🂡

Live poker analysing tool.
Make best play out of data.

## Ideas

- [ ] Extract live playing data from different provider (Winamax, Pokerstars, ...)
- [ ] Transform data into relevant stats
- [ ] Display stats in a Jupyter notebook
- [ ] Display live stats on a HUD 
