use std::fmt;

#[derive(Debug, PartialEq)]
pub enum Provider {
    Winamax,
}

impl fmt::Display for Provider {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{:?}", self)
    }
}
