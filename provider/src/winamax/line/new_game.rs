use std::error;
use std::fmt;
use std::num::ParseFloatError;

use chrono::prelude::*;
use logos::{Lexer, Logos};

use core::model::game::Game;

use crate::common::Enricher;
use crate::provider::Provider;

pub struct NewGameParser {}

impl Enricher<Game> for NewGameParser {
    type Err = NewGameParseError;
    fn enrich(mut game: Game, line: &str) -> Result<Game, Self::Err> {
        let mut lex = NewGameToken::lexer(line);

        // Provider
        if let Some(NewGameToken::Provider) = lex.next() {
            game.provider = Provider::Winamax.to_string();
        } else {
            return Err(NewGameParseError::InvalidProvider(String::from(line)));
        }

        // Split
        lex.next();

        // Game type
        if let Some(NewGameToken::GameType) = lex.next() {
            game.game_type = lex.slice().to_string();
        } else {
            return Err(NewGameParseError::InvalidGameType(String::from(line)));
        }


        // Game details + split
        game.game_details = String::from("");
        while let Some(token) = lex.next() {
            match token {
                NewGameToken::Split => break,
                _ => game.game_details.push_str(lex.slice()),
            }
        }

        // Provider game id
        if let Some(NewGameToken::HandId(id)) = lex.next() {
            game.provider_game_id = id;
        } else {
            return Err(NewGameParseError::InvalidGameProviderId(String::from(line)));
        }

        // Split
        lex.next();

        // Variant
        if let Some(NewGameToken::Variant) = lex.next() {
            game.variant = lex.slice().to_string();
        } else {
            return Err(NewGameParseError::InvalidVariant(String::from(line)));
        }

        // Whitespace
        lex.next();

        // Blinds
        if let Some(NewGameToken::Blinds((small_blind, big_blind))) = lex.next() {
            game.small_blind = small_blind;
            game.big_blind = big_blind;
        } else {
            return Err(NewGameParseError::InvalidBlinds(String::from(line)));
        }

        // Split
        lex.next();

        // Date
        if let Some(NewGameToken::Date(date)) = lex.next() {
            game.started_at = date;
        } else {
            return Err(NewGameParseError::InvalidStartDate(String::from(line)));
        }

        Ok(game)
    }
}

#[derive(Debug, PartialEq)]
pub enum NewGameParseError {
    InvalidProvider(String),
    InvalidGameType(String),
    InvalidGameProviderId(String),
    InvalidVariant(String),
    InvalidBlinds(String),
    InvalidStartDate(String),
}

impl fmt::Display for NewGameParseError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            NewGameParseError::InvalidProvider(ref s) => write!(f, "Failed to parse provider: '{}'", s),
            NewGameParseError::InvalidGameType(ref s) => write!(f, "Failed to parse game type: '{}'", s),
            NewGameParseError::InvalidGameProviderId(ref s) => write!(f, "Failed to parse game provider id: '{}'", s),
            NewGameParseError::InvalidVariant(ref s) => write!(f, "Failed to parse variant: '{}'", s),
            NewGameParseError::InvalidBlinds(ref s) => write!(f, "Failed to parse blinds: '{}'", s),
            NewGameParseError::InvalidStartDate(ref s) => write!(f, "Failed to parse start date: '{}'", s),
        }
    }
}

impl error::Error for NewGameParseError {
    fn source(&self) -> Option<&(dyn error::Error + 'static)> {
        None
    }
}


#[derive(Logos, Debug, PartialEq, Clone)]
enum NewGameToken {
    #[token("Winamax Poker")]
    Provider,

    #[token("CashGame")]
    #[token("Tournament")]
    GameType,

    #[regex(r"HandId: #\d+-\d+-\d+", NewGameToken::strip_hand_id_prefix)]
    HandId(String),

    #[token("Holdem no limit")]
    Variant,

    // (small_blind, big_blind)
    #[regex(r"\(\d+\.*\d*/\d+\.*\d*\)", NewGameToken::parse_blinds)]
    Blinds((f32, f32)),

    #[regex(r"\d+/\d+/\d+ \d+:\d+:\d+ UTC", NewGameToken::parse_datetime)]
    Date(DateTime<Utc>),

    #[token(" - ")]
    Split,

    #[token(" ")]
    Whitespace,

    #[error]
    Error,
}

impl NewGameToken {
    fn strip_hand_id_prefix(lex: &mut Lexer<NewGameToken>) -> Option<String> {
        let slice = lex.slice();
        // Remove 'HandId: '
        Some(String::from(&slice["HandId: ".len()..]))
    }

    fn parse_datetime(lex: &mut Lexer<NewGameToken>) -> Result<DateTime<Utc>, chrono::ParseError> {
        let slice = lex.slice();
        let format = "%Y/%m/%d %H:%M:%S UTC";
        match NaiveDateTime::parse_from_str(slice, format) {
            Ok(date_time) => Ok(Utc.from_utc_datetime(&date_time)),
            Err(error) => Err(error),
        }
    }

    fn parse_blinds(lex: &mut Lexer<NewGameToken>) -> Result<(f32, f32), ParseFloatError> {
        let slice = lex.slice();
        // Remove first '(' and last ')'
        let cleaned_slice = &slice[1..slice.len() - 1];
        let blinds: Vec<&str> = cleaned_slice.split("/").collect();
        match (blinds[0].parse::<f32>(), blinds[1].parse::<f32>()) {
            (Ok(small_blind), Ok(big_blind)) => Ok((small_blind, big_blind)),
            (Err(sm_err), _) => Err(sm_err),
            (_, Err(big_err)) => Err(big_err),
        }
    }
}


#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn should_parse_game_correctly() {
        let input = "Winamax Poker - Tournament \"Hold'em [170 Max]\" buyIn: 1.81€ + 0.20€ level: - HandId: #12345625-201-123456074 - Holdem no limit (0.01/0.02) - 2020/04/01 17:04:34 UTC";
        let mut expected = Game::new();
        expected.provider = String::from("Winamax");
        expected.game_type = String::from("Tournament");
        expected.game_details = String::from(" \"Hold'em [170 Max]\" buyIn: 1.81€ + 0.20€ level:");
        expected.provider_game_id = String::from("#12345625-201-123456074");
        expected.small_blind = 0.01;
        expected.big_blind = 0.02;
        expected.started_at = Utc.ymd(2020, 4, 1).and_hms(17, 4, 34);

        let result = NewGameParser::enrich(Game::new(), input);
        assert_eq!(Ok(expected), result);
    }

    #[test]
    fn should_return_error_on_wrong_provider() {
        let input = "Winmx Poker - Tournament \"Hold'em [170 Max]\" buyIn: 1.81€ + 0.20€ level: - HandId: #12345625-201-123456074 - Holdem no limit (0.01/0.02) - 2020/04/01 17:04:34 UTC";
        let result = NewGameParser::enrich(Game::new(), input);
        assert_eq!(Err(NewGameParseError::InvalidProvider(String::from(input))), result);
    }

    #[test]
    fn should_return_error_on_wrong_game_type() {
        let input = "Winamax Poker - Tourn \"Hold'em [170 Max]\" buyIn: 1.81€ + 0.20€ level: - HandId: #12345625-201-123456074 - Holdem no limit (0.01/0.02) - 2020/04/01 17:04:34 UTC";
        let result = NewGameParser::enrich(Game::new(), input);
        assert_eq!(Err(NewGameParseError::InvalidGameType(String::from(input))), result);
    }

    #[test]
    fn should_return_error_on_wrong_game_provider_id() {
        let input = "Winamax Poker - Tournament \"Hold'em [170 Max]\" buyIn: 1.81€ + 0.20€ level: - HandId: #12345625-abc-1234-56-074 - Holdem no limit (0.01/0.02) - 2020/04/01 17:04:34 UTC";
        let result = NewGameParser::enrich(Game::new(), input);
        assert_eq!(Err(NewGameParseError::InvalidGameProviderId(String::from(input))), result);
    }

    #[test]
    fn should_return_error_on_wrong_game_variant() {
        let input = "Winamax Poker - Tournament \"Hold'em [170 Max]\" buyIn: 1.81€ + 0.20€ level: - HandId: #12345625-201-123456074- UNKNOWN_VARIANT (0.01/0.02) - 2020/04/01 17:04:34 UTC";
        let result = NewGameParser::enrich(Game::new(), input);
        assert_eq!(Err(NewGameParseError::InvalidVariant(String::from(input))), result);
    }

    #[test]
    fn should_return_error_on_wrong_game_blinds() {
        let input = "Winamax Poker - Tournament \"Hold'em [170 Max]\" buyIn: 1.81€ + 0.20€ level: - HandId: #12345625-201-123456074 - Holdem no limit (0.01) - 2020/04/01 17:04:34 UTC";
        let result = NewGameParser::enrich(Game::new(), input);
        assert_eq!(Err(NewGameParseError::InvalidBlinds(String::from(input))), result);
    }

    #[test]
    fn should_return_error_on_wrong_game_date() {
        let input = "Winamax Poker - Tournament \"Hold'em [170 Max]\" buyIn: 1.81€ + 0.20€ level: - HandId: #12345625-201-123456074 - Holdem no limit (0.01/0.02) - 2020/04/00 00:34 UTC";
        let result = NewGameParser::enrich(Game::new(), input);
        assert_eq!(Err(NewGameParseError::InvalidStartDate(String::from(input))), result);
    }


    #[test]
    fn should_parse_tournament_new_game_line() {
        let input = "Winamax Poker - Tournament \"Hold'em [170 Max]\" buyIn: 1.81€ + 0.20€ level: - HandId: #12345625-201-123456074 - Holdem no limit (0.01/0.02) - 2020/04/01 17:04:34 UTC";
        let mut lex = NewGameToken::lexer(input);

        // Provider
        assert_eq!(Some(NewGameToken::Provider), lex.next());
        assert_eq!("Winamax Poker", lex.slice());

        // Split
        assert_eq!(Some(NewGameToken::Split), lex.next());
        assert_eq!(" - ", lex.slice());

        // GameType
        assert_eq!(Some(NewGameToken::GameType), lex.next());
        assert_eq!("Tournament", lex.slice());

        // GameDetails
        let mut game_details: String = String::from("");
        while let Some(token) = lex.next() {
            match token {
                NewGameToken::Split => break,
                _ => game_details.push_str(lex.slice()),
            }
        }
        assert_eq!(
            "\"Hold'em [170 Max]\" buyIn: 1.81€ + 0.20€ level:",
            game_details.trim()
        );

        // HandId
        assert_eq!(
            Some(NewGameToken::HandId(String::from(
                "#12345625-201-123456074"
            ))),
            lex.next()
        );
        assert_eq!("HandId: #12345625-201-123456074", lex.slice());

        // Split
        assert_eq!(Some(NewGameToken::Split), lex.next());
        assert_eq!(" - ", lex.slice());

        // Variant
        assert_eq!(Some(NewGameToken::Variant), lex.next());
        assert_eq!("Holdem no limit", lex.slice());

        // Whitespace
        assert_eq!(Some(NewGameToken::Whitespace), lex.next());

        // Small Blind
        assert_eq!(Some(NewGameToken::Blinds((0.01f32, 0.02f32))), lex.next());
        assert_eq!("(0.01/0.02)", lex.slice());

        // Split
        assert_eq!(Some(NewGameToken::Split), lex.next());
        assert_eq!(" - ", lex.slice());

        // Date
        assert_eq!(
            Some(NewGameToken::Date(Utc.ymd(2020, 4, 1).and_hms(17, 4, 34))),
            lex.next()
        );
        assert_eq!("2020/04/01 17:04:34 UTC", lex.slice());
    }

    #[test]
    fn should_parse_cashgame_new_game_line() {
        let input = "Winamax Poker - CashGame - HandId: #12345625-201-123456074 - Holdem no limit (0.01/0.02) - 2020/04/01 17:04:34 UTC";
        let mut lex = NewGameToken::lexer(input);

        // Provider
        assert_eq!(Some(NewGameToken::Provider), lex.next());
        assert_eq!("Winamax Poker", lex.slice());

        // Split
        assert_eq!(Some(NewGameToken::Split), lex.next());
        assert_eq!(" - ", lex.slice());

        // GameType
        assert_eq!(Some(NewGameToken::GameType), lex.next());
        assert_eq!("CashGame", lex.slice());

        // GameDetails
        let mut game_details: String = "".to_owned();
        while let Some(token) = lex.next() {
            match token {
                NewGameToken::Split => break,
                _ => game_details.push_str(lex.slice()),
            }
        }
        assert_eq!("", game_details.trim());

        // HandId
        assert_eq!(
            Some(NewGameToken::HandId(String::from(
                "#12345625-201-123456074"
            ))),
            lex.next()
        );
        assert_eq!("HandId: #12345625-201-123456074", lex.slice());

        // Split
        assert_eq!(Some(NewGameToken::Split), lex.next());
        assert_eq!(" - ", lex.slice());

        // Variant
        assert_eq!(Some(NewGameToken::Variant), lex.next());
        assert_eq!("Holdem no limit", lex.slice());

        // Whitespace
        assert_eq!(Some(NewGameToken::Whitespace), lex.next());
        // Small Blind
        assert_eq!(Some(NewGameToken::Blinds((0.01f32, 0.02f32))), lex.next());
        assert_eq!("(0.01/0.02)", lex.slice());

        // Split
        assert_eq!(Some(NewGameToken::Split), lex.next());
        assert_eq!(" - ", lex.slice());

        // Date
        assert_eq!(
            Some(NewGameToken::Date(Utc.ymd(2020, 4, 1).and_hms(17, 4, 34))),
            lex.next()
        );
        assert_eq!("2020/04/01 17:04:34 UTC", lex.slice());
    }
}
