use std::num::ParseIntError;

use logos::{Lexer, Logos};

use core::model::game::Game;

use crate::common::Enricher;
use std::{fmt, error};

pub struct TableParser {}

impl Enricher<Game> for TableParser {
    type Err = TableParseError;

    fn enrich(mut game: Game, line: &str) -> Result<Game, Self::Err> {
        let mut lex = TableToken::lexer(line);

        // Table first token
        if Some(TableToken::Table) != lex.next() {
            return Err(TableParseError::InvalidTableToken(String::from(line)))
        }

        // TableName + Player max
        let mut buffer: String = String::from("");
        let mut player_max_token = TableToken::Error;
        while let Some(token) = lex.next() {
            match token {
                TableToken::PlayerMax(value) => {
                    player_max_token = TableToken::PlayerMax(value);
                    break;
                }
                _ => buffer.push_str(lex.slice()),
            }
        }
        game.table_name = String::from(&buffer[" '".len()..buffer.len() - "' ".len()]);

        if let TableToken::PlayerMax(player_max) = player_max_token {
            game.player_max = player_max;
        } else {
            return Err(TableParseError::InvalidPlayerMax(String::from(line)))
        }

        // WhiteSpace
        lex.next();

        // Money Type
        if let Some(TableToken::MoneyType(money_type)) = lex.next() {
            game.money_type = money_type;
        } else {
            return Err(TableParseError::InvalidMoneyType(String::from(line)))
        }

        // WhiteSpace
        lex.next();

        // Button seat #
        if let Some(TableToken::ButtonSeat(button_seat)) = lex.next() {
            game.button_seat_index = button_seat - 1;
        } else {
            return Err(TableParseError::InvalidButtonSeat(String::from(line)))
        }

        // Initialize seats vector
        game.seats = vec![None; game.player_max as usize];

        Ok(game)
    }
}

#[derive(Debug, PartialEq)]
pub enum TableParseError {
    InvalidTableToken(String),
    InvalidPlayerMax(String),
    InvalidMoneyType(String),
    InvalidButtonSeat(String),
}

impl fmt::Display for TableParseError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            TableParseError::InvalidTableToken(ref s) => write!(f, "Failed to parse table first token: '{}'", s),
            TableParseError::InvalidPlayerMax(ref s) => write!(f, "Failed to parse table player max: '{}'", s),
            TableParseError::InvalidMoneyType(ref s) => write!(f, "Failed to parse table money type: '{}'", s),
            TableParseError::InvalidButtonSeat(ref s) => write!(f, "Failed to parse table button seat: '{}'", s),
        }
    }
}

impl error::Error for TableParseError {
    fn source(&self) -> Option<&(dyn error::Error + 'static)> {
        None
    }
}

#[derive(Logos, Debug, PartialEq, Clone)]
enum TableToken {
    #[token("Table:")]
    Table,

    #[regex(r"\d+\-max", TableToken::parse_max_player)]
    PlayerMax(usize),

    #[token("(real money)", TableToken::trim_first_and_last_char)]
    #[token("(play money)", TableToken::trim_first_and_last_char)]
    MoneyType(String),

    #[regex(r"Seat #\d+ is the button", TableToken::parse_button_seat)]
    ButtonSeat(usize),

    #[token(" ")]
    Whitespace,

    #[error]
    Error,
}

impl TableToken {
    fn trim_first_and_last_char(lex: &mut Lexer<TableToken>) -> Option<String> {
        let slice = lex.slice();
        // Remove last chars '-max'
        Some(String::from(&slice[1..slice.len() - 1]))
    }

    fn parse_max_player(lex: &mut Lexer<TableToken>) -> Result<usize, ParseIntError> {
        let slice = lex.slice();
        // Remove last chars '-max'
        slice[0..slice.len() - "-max".len()].parse()
    }

    fn parse_button_seat(lex: &mut Lexer<TableToken>) -> Result<usize, ParseIntError> {
        let slice = lex.slice();
        // Remove last chars 'Seat #' and ' is the button'
        slice["Seat #".len()..(slice.len() - " is the button".len())].parse()
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn should_parse_table() {
        let input = "Table: 'Hold'em [170 Max](123456789)#14' 6-max (real money) Seat #5 is the button";
        let mut expected = Game::new();
        expected.table_name = String::from("'Hold'em [170 Max](123456789)#14'");
        expected.player_max = 6;
        expected.money_type = String::from("real money");
        expected.button_seat_index = 5;
        expected.seats = vec![None; 6];

        let result = TableParser::enrich(Game::new(), input);
        assert_eq!(Ok(expected), result);
    }

    #[test]
    fn should_return_error_on_wrong_table_token() {
        let input = "UNKNOWN_TOKEN: 'Hold'em [170 Max](123456789)#14' 6-max (real money) Seat #5 is the button";
        let result = TableParser::enrich(Game::new(), input);
        assert_eq!(Err(TableParseError::InvalidTableToken(String::from(input))), result);
    }

    #[test]
    fn should_return_error_on_wrong_table_player_max() {
        let input = "Table: 'Hold'em [170 Max](123456789)#14' a-max (real money) Seat #5 is the button";
        let result = TableParser::enrich(Game::new(), input);
        assert_eq!(Err(TableParseError::InvalidPlayerMax(String::from(input))), result);
    }

    #[test]
    fn should_return_error_on_wrong_table_money_type() {
        let input = "Table: 'Hold'em [170 Max](123456789)#14' 6-max (unknown money) Seat #5 is the button";
        let result = TableParser::enrich(Game::new(), input);
        assert_eq!(Err(TableParseError::InvalidMoneyType(String::from(input))), result);
    }

    #[test]
    fn should_return_error_on_wrong_button_seat() {
        let input = "Table: 'Hold'em [170 Max](123456789)#14' 6-max (real money) Seat #UNKNOWN_BUTTON_INDEX is the button";
        let result = TableParser::enrich(Game::new(), input);
        assert_eq!(Err(TableParseError::InvalidButtonSeat(String::from(input))), result);
    }

    #[test]
    fn should_parse_tournament_table_line() {
        let input = "Table: 'Hold'em [170 Max](123456789)#14' 6-max (real money) Seat #5 is the button";
        let mut lex = TableToken::lexer(input);

        // Table
        assert_eq!(Some(TableToken::Table), lex.next());
        assert_eq!("Table:", lex.slice());

        // TableName
        let mut buffer: String = String::from("");
        let mut player_max_token = TableToken::Error;
        while let Some(token) = lex.next() {
            match token {
                TableToken::PlayerMax(value) => {
                    player_max_token = TableToken::PlayerMax(value);
                    break;
                }
                _ => buffer.push_str(lex.slice()),
            }
        }
        let table_name = &buffer[" '".len()..buffer.len() - "' ".len()];
        assert_eq!("Hold'em [170 Max](123456789)#14", table_name);
        // PlayerMax
        assert_eq!(TableToken::PlayerMax(6usize), player_max_token);
        assert_eq!("6-max", lex.slice());

        // WhiteSpace
        assert_eq!(Some(TableToken::Whitespace), lex.next());

        // PlayerMax
        assert_eq!(
            Some(TableToken::MoneyType(String::from("real money"))),
            lex.next()
        );
        assert_eq!("(real money)", lex.slice());

        // WhiteSpace
        assert_eq!(Some(TableToken::Whitespace), lex.next());

        // PlayerMax
        assert_eq!(Some(TableToken::ButtonSeat(5usize)), lex.next());
        assert_eq!("Seat #5 is the button", lex.slice());
    }

    #[test]
    fn should_parse_cashgame_table_line() {
        let input = "Table: 'Wichita 250' 5-max (play money) Seat #3 is the button";
        let mut lex = TableToken::lexer(input);

        // Table
        assert_eq!(Some(TableToken::Table), lex.next());
        assert_eq!("Table:", lex.slice());

        // TableName
        let mut buffer: String = String::from("");
        let mut player_max_token = TableToken::Error;
        while let Some(token) = lex.next() {
            match token {
                TableToken::PlayerMax(_) => {
                    player_max_token = token;
                    break;
                }
                _ => buffer.push_str(lex.slice()),
            }
        }
        let table_name = &buffer[" '".len()..buffer.len() - "' ".len()];
        assert_eq!("Wichita 250", table_name);

        // PlayerMax
        assert_eq!(TableToken::PlayerMax(5usize), player_max_token);
        assert_eq!("5-max", lex.slice());

        // WhiteSpace
        assert_eq!(Some(TableToken::Whitespace), lex.next());

        // PlayerMax
        assert_eq!(
            Some(TableToken::MoneyType(String::from("play money"))),
            lex.next()
        );
        assert_eq!("(play money)", lex.slice());

        // WhiteSpace
        assert_eq!(Some(TableToken::Whitespace), lex.next());

        // PlayerMax
        assert_eq!(Some(TableToken::ButtonSeat(3usize)), lex.next());
        assert_eq!("Seat #3 is the button", lex.slice());
    }
}
