use std::{error, fmt};

use logos::{Lexer, Logos};

use core::model::board::{Board, Flop, Turn};
use core::model::game::Game;

use crate::common::{Enricher, Parser};
use crate::winamax::model::board::FlopParseError;
use crate::winamax::parser::WinamaxParser;
use core::model::card::card::Card;
use crate::winamax::model::card::CardParseError;

pub struct RiverHeader {}

impl Enricher<Game> for RiverHeader {
    type Err = RiverHeaderParseError;

    fn enrich(mut game: Game, line: &str) -> Result<Game, Self::Err> {
        let mut lex = RiverHeaderToken::lexer(line);

        let turn = match lex.next() {
            Some(RiverHeaderToken::Header(turn)) => turn,
            _ => return Err(RiverHeaderParseError::InvalidRiverHeader(String::from(line)))
        };

        game.board.turn = Some(turn);
        Ok(game)
    }
}

#[derive(Debug, PartialEq)]
pub enum RiverHeaderParseError {
    InvalidRiverHeader(String),
}

impl fmt::Display for RiverHeaderParseError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            RiverHeaderParseError::InvalidRiverHeader(ref s) => write!(f, "Failed to parse River header: '{}'", s),
        }
    }
}

impl error::Error for RiverHeaderParseError {
    fn source(&self) -> Option<&(dyn error::Error + 'static)> {
        None
    }
}

#[derive(Logos, Debug, PartialEq, Clone)]
enum RiverHeaderToken {
    #[regex(r"\*\*\* RIVER \*\*\* \[\w\w \w\w \w\w \w\w]\[\w\w\]", RiverHeaderToken::strip_header_and_parse)]
    Header(Turn),

    #[error]
    Error,
}

impl RiverHeaderToken {
    fn strip_header_and_parse(lex: &mut Lexer<RiverHeaderToken>) -> Result<Turn, CardParseError> {
        let slice = lex.slice();
        let card = &slice["*** RIVER *** [xx xx xx xx][".len()..slice.len() - 1];
        WinamaxParser::parse(card)
    }
}

#[cfg(test)]
mod test {
    use super::*;
    use core::model::card::card::Card;
    use core::model::card::rank::Rank;
    use core::model::card::suit::Suit;

    #[test]
    fn should_parse_river_header() {
        let input = "*** RIVER *** [7c 3s 6h Qs][2h]";
        let mut expected = Game::new();
        expected.board.turn = Some(Card::new(Rank::Two, Suit::Heart));
        let result = RiverHeader::enrich(Game::new(), input);
        assert_eq!(Ok(expected), result)
    }

    #[test]
    fn should_return_error_on_invalid_header() {
        let input = "*** INVALID_HEADER ***";
        let result = RiverHeader::enrich(Game::new(), input);
        assert_eq!(Err(RiverHeaderParseError::InvalidRiverHeader(String::from(input))), result)
    }

    #[test]
    fn should_return_error_when_turn_header_contains_invalid_card() {
        let input = "*** RIVER *** [7c 3s 6h Qs][12c]";
        let result = RiverHeader::enrich(Game::new(), input);
        assert_eq!(Err(RiverHeaderParseError::InvalidRiverHeader(String::from(input))), result);
    }
}
