use std::{error, fmt};

use logos::{Lexer, Logos};

use core::model::game::Game;
use core::model::hand::Hand;

use crate::common::{Enricher, Parser};
use crate::winamax::model::hand::HandParseError;
use crate::winamax::parser::WinamaxParser;

pub struct CardPlayerDealtParser {}


// Dealt to Seel [Qh 8s]
impl Enricher<Game> for CardPlayerDealtParser {
    type Err = CardPlayerDealtParseError;

    fn enrich(mut game: Game, line: &str) -> Result<Game, Self::Err> {
        let mut lex = CardPlayerDealtToken::lexer(line);

        // Deal to
        if Some(CardPlayerDealtToken::DealtTo) != lex.next() {
            return Err(CardPlayerDealtParseError::InvalidDealtToAction(String::from(line)));
        }

        // Player name + Hand
        let mut player_name: String = String::from("");
        let mut hand_opt: Option<Hand> = None;
        while let Some(token) = lex.next() {
            match token {
                CardPlayerDealtToken::Hand(hand) => {
                    hand_opt = Some(hand);
                    break;
                }
                _ => player_name.push_str(lex.slice()),
            }
        }

        let hand = if let Some(value) = hand_opt {
            value
        } else {
            return Err(CardPlayerDealtParseError::InvalidHandDealt(String::from(line)));
        };


        todo!("\
        Fix following code to update player without moving references.\
        Tracks:
            1. Modify model using reference?
            2. Modify below code without moving game ref
        ");
        game.seats
            .into_iter()
            .filter_map(|player| player)
            .filter(|player| player_name == player.name)
            .for_each(|mut player| player.hand = Some(hand));

        Ok(game)
    }
}

#[derive(Debug, PartialEq)]
pub enum CardPlayerDealtParseError {
    InvalidDealtToAction(String),
    InvalidHandDealt(String),
}

impl fmt::Display for CardPlayerDealtParseError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            CardPlayerDealtParseError::InvalidDealtToAction(ref s) => write!(f, "Failed to parse dealt header: '{}'", s),
            CardPlayerDealtParseError::InvalidHandDealt(ref s) => write!(f, "Failed to parse hand dealt to player: '{}'", s),
        }
    }
}

impl error::Error for CardPlayerDealtParseError {
    fn source(&self) -> Option<&(dyn error::Error + 'static)> {
        None
    }
}


#[derive(Logos, Debug, PartialEq, Clone)]
pub enum CardPlayerDealtToken {
    #[token("Dealt to")]
    DealtTo,
    #[regex(r"\[\w\w \w\w\]", CardPlayerDealtToken::parse_hand)]
    Hand(Hand),
    #[error]
    Error,
}

impl CardPlayerDealtToken {
    fn parse_hand(lex: &mut Lexer<CardPlayerDealtToken>) -> Result<Hand, HandParseError> {
        let slice = lex.slice();
        match WinamaxParser::parse(slice) {
            Ok(fold) => Ok(fold),
            Err(err) => Err(err)
        }
    }
}

#[cfg(test)]
mod test {
    use core::model::card::card::Card;
    use core::model::card::rank::Rank;
    use core::model::card::suit::Suit;
    use core::model::player::Player;

    use super::*;

    #[test]
    // TODO: Make this test work
    #[ignore]
    fn should_parse_cards_dealt_to_player_correctly() {
        let input = "Dealt to Seel [Qh 8s]";
        let mut input_game = Game::new();
        input_game.seats.push(Some(Player::new(String::from("Seel"), None, 500.0)));
        let mut expected = Game::new();
        expected.seats.push(Some(
            Player::new(
                String::from("Seel"),
                Some(Hand::new(
                    Card::new(Rank::Queen, Suit::Heart),
                    Card::new(Rank::Eight, Suit::Spade),
                )),
                500.0,
            )
        ));
        let result = CardPlayerDealtParser::enrich(Game::new(), input);
        assert_eq!(Ok(expected), result);
    }
    // TODO tests...
}
