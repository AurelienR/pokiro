use std::{error, fmt};

use logos::{Lexer, Logos};

use core::model::board::{Board, Flop, Turn};
use core::model::game::Game;

use crate::common::{Enricher, Parser};
use crate::winamax::model::board::FlopParseError;
use crate::winamax::parser::WinamaxParser;
use core::model::card::card::Card;
use crate::winamax::model::card::CardParseError;

pub struct TurnHeader {}

impl Enricher<Game> for TurnHeader {
    type Err = TurnHeaderParseError;

    fn enrich(mut game: Game, line: &str) -> Result<Game, Self::Err> {
        let mut lex = TurnHeaderToken::lexer(line);

        let turn = match lex.next() {
            Some(TurnHeaderToken::Header(turn)) => turn,
            _ => return Err(TurnHeaderParseError::InvalidTurnHeader(String::from(line)))
        };

        game.board.turn = Some(turn);
        Ok(game)
    }
}

#[derive(Debug, PartialEq)]
pub enum TurnHeaderParseError {
    InvalidTurnHeader(String),
}

impl fmt::Display for TurnHeaderParseError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            TurnHeaderParseError::InvalidTurnHeader(ref s) => write!(f, "Failed to parse Turn header: '{}'", s),
        }
    }
}

impl error::Error for TurnHeaderParseError {
    fn source(&self) -> Option<&(dyn error::Error + 'static)> {
        None
    }
}

// *** TURN *** [7c 3s 6h][Qs]
#[derive(Logos, Debug, PartialEq, Clone)]
enum TurnHeaderToken {
    #[regex(r"\*\*\* TURN \*\*\* \[\w\w \w\w \w\w\]\[\w\w\]", TurnHeaderToken::strip_header_and_parse)]
    Header(Turn),

    #[error]
    Error,
}

impl TurnHeaderToken {
    fn strip_header_and_parse(lex: &mut Lexer<TurnHeaderToken>) -> Result<Turn, CardParseError> {
        let slice = lex.slice();
        let card = &slice["*** TURN *** [xx xx xx][".len()..slice.len() - 1];
        WinamaxParser::parse(card)
    }
}

#[cfg(test)]
mod test {
    use super::*;
    use core::model::card::card::Card;
    use core::model::card::rank::Rank;
    use core::model::card::suit::Suit;

    #[test]
    fn should_parse_turn_header() {
        let input = "*** TURN *** [7c 3s 6h][Qs]";
        let mut expected = Game::new();
        expected.board.turn = Some(Card::new(Rank::Queen, Suit::Spade));
        let result = TurnHeader::enrich(Game::new(), input);
        assert_eq!(Ok(expected), result)
    }

    #[test]
    fn should_return_error_on_invalid_header() {
        let input = "*** INVALID_HEADER ***";
        let result = TurnHeader::enrich(Game::new(), input);
        assert_eq!(Err(TurnHeaderParseError::InvalidTurnHeader(String::from(input))), result)
    }

    #[test]
    fn should_return_error_when_turn_header_contains_invalid_card() {
        let input = "*** TURN *** [7c 3s 6h][Qb]";
        let result = TurnHeader::enrich(Game::new(), input);
        assert_eq!(Err(TurnHeaderParseError::InvalidTurnHeader(String::from(input))), result);
    }
}
