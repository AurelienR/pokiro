use std::{error, fmt};

use logos::{Lexer, Logos};

use core::model::action::action::Action;
use core::model::action::bet::Bet;
use core::model::action::call::Call;
use core::model::action::check::Check;
use core::model::action::fold::Fold;
use core::model::action::raise::Raise;
use core::model::event::Event;
use core::model::game::Game;
use core::model::phase::Phase;

use crate::common::{Enricher, Parser};
use crate::winamax::model::action::bet::BetParseError;
use crate::winamax::model::action::call::CallParseError;
use crate::winamax::model::action::check::CheckParseError;
use crate::winamax::model::action::fold::FoldParseError;
use crate::winamax::model::action::raise::RaiseParseError;
use crate::winamax::parser::WinamaxParser;
use core::model::action::phase_start::PhaseStart;

pub struct PlayerActionParser {}

impl Enricher<Game> for PlayerActionParser {
    type Err = PlayerActionParseError;

    fn enrich(mut game: Game, line: &str) -> Result<Game, Self::Err> {
        let mut lex = PlayerActionToken::lexer(line);

        // Player + action
        let mut player_name: String = String::from("");
        let mut action_opt: Option<Box<dyn Action>> = None;
        while let Some(token) = lex.next() {
            match token {
                PlayerActionToken::Fold(mut fold) => {
                    fold.player = player_name.trim().to_string();
                    action_opt = Some(Box::new(fold));
                    break;
                }
                PlayerActionToken::Check(mut check) => {
                    check.player = player_name.trim().to_string();
                    action_opt = Some(Box::new(check));
                    break;
                }
                PlayerActionToken::Call(mut call) => {
                    call.player = player_name.trim().to_string();
                    action_opt = Some(Box::new(call));
                    break;
                }
                PlayerActionToken::Bet(mut bet) => {
                    bet.player = player_name.trim().to_string();
                    action_opt = Some(Box::new(bet));
                    break;
                }
                PlayerActionToken::Raise(mut raise) => {
                    raise.player = player_name.trim().to_string();
                    action_opt = Some(Box::new(raise));
                    break;
                }
                _ => player_name.push_str(lex.slice()),
            }
        }

        let action = match action_opt {
            Some(value) => value,
            None => return Err(PlayerActionParseError::InvalidPlayerAction(String::from(line)))
        };

        // Retrieve last event phase
        let last_phase = match game.history.last() {
            Some(event) => event.phase.clone(),
            None => return Err(PlayerActionParseError::UnableToDetermineCurrentGamePhase(String::from(line))),
        };

        // Push player action event
        let event = Event::new(last_phase, action);
        game.history.push(event);

        Ok(game)
    }
}


#[derive(Debug, PartialEq)]
pub enum PlayerActionParseError {
    InvalidPlayerAction(String),
    UnableToDetermineCurrentGamePhase(String),
}

impl fmt::Display for PlayerActionParseError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            PlayerActionParseError::InvalidPlayerAction(ref s) => write!(f, "Failed to parse player action: '{}'", s),
            PlayerActionParseError::UnableToDetermineCurrentGamePhase(ref s) => write!(f, "Failed to get last history event phase: '{}'", s),
        }
    }
}

impl error::Error for PlayerActionParseError {
    fn source(&self) -> Option<&(dyn error::Error + 'static)> {
        None
    }
}


// Sandshrew calls 0.02
// Growlithe folds
// Squirtle checks
// Snorlax raises 1.96 to 1.98 and is all-in
// Pikachu calls 1.96 and is all-in
// Nidoran bets 0.02
#[derive(Logos, Debug, PartialEq, Clone)]
pub enum PlayerActionToken {
    #[token("folds", PlayerActionToken::parse_fold)]
    Fold(Fold),
    #[token("checks", PlayerActionToken::parse_check)]
    Check(Check),
    #[regex(r"calls \d+\.*\d*", PlayerActionToken::parse_call)]
    #[regex(r"calls \d+\.*\d* and is all-in", PlayerActionToken::parse_call)]
    Call(Call),
    #[regex(r"bets \d+\.*\d*", PlayerActionToken::parse_bet)]
    #[regex(r"bets \d+\.*\d* and is all-in", PlayerActionToken::parse_bet)]
    Bet(Bet),
    #[regex(r"raises \d+\.*\d* to \d+\.*\d*", PlayerActionToken::parse_raise)]
    #[regex(r"raises \d+\.*\d* to \d+\.*\d* and is all-in", PlayerActionToken::parse_raise)]
    Raise(Raise),

    #[error]
    Error,
}

impl PlayerActionToken {
    fn parse_fold(lex: &mut Lexer<PlayerActionToken>) -> Result<Fold, FoldParseError> {
        let slice = lex.slice();
        match WinamaxParser::parse(slice) {
            Ok(fold) => Ok(fold),
            Err(err) => Err(err)
        }
    }

    fn parse_check(lex: &mut Lexer<PlayerActionToken>) -> Result<Check, CheckParseError> {
        let slice = lex.slice();
        match WinamaxParser::parse(slice) {
            Ok(check) => Ok(check),
            Err(err) => Err(err)
        }
    }

    fn parse_call(lex: &mut Lexer<PlayerActionToken>) -> Result<Call, CallParseError> {
        let slice = lex.slice();
        match WinamaxParser::parse(slice) {
            Ok(call) => Ok(call),
            Err(err) => Err(err)
        }
    }

    fn parse_bet(lex: &mut Lexer<PlayerActionToken>) -> Result<Bet, BetParseError> {
        let slice = lex.slice();
        match WinamaxParser::parse(slice) {
            Ok(bet) => Ok(bet),
            Err(err) => Err(err)
        }
    }

    fn parse_raise(lex: &mut Lexer<PlayerActionToken>) -> Result<Raise, RaiseParseError> {
        let slice = lex.slice();
        match WinamaxParser::parse(slice) {
            Ok(raise) => Ok(raise),
            Err(err) => Err(err)
        }
    }
}

#[cfg(test)]
mod test {
    use super::*;
    use core::model::action::phase_start::PhaseStart;

    #[test]
    fn should_parse_player_fold_correctly() {
        let input = "Growlithe folds";
        let mut input_game = Game::new();
        input_game.history.push(Event::new(Phase::PreFlop, Box::new(PhaseStart::new())));
        let mut expected = Game::new();
        expected.history.push(Event::new(Phase::PreFlop, Box::new(PhaseStart::new())));
        expected.history.push(Event::new(Phase::PreFlop, Box::new(Fold::new(String::from("Growlithe")))));

        let result = PlayerActionParser::enrich(input_game, input);
        assert_eq!(Ok(expected), result);
    }

    #[test]
    fn should_parse_player_check_correctly() {
        let input = "Squirtle checks";
        let mut input_game = Game::new();
        input_game.history.push(Event::new(Phase::Flop, Box::new(PhaseStart::new())));
        let mut expected = Game::new();
        expected.history.push(Event::new(Phase::Flop, Box::new(PhaseStart::new())));
        expected.history.push(Event::new(Phase::Flop, Box::new(Check::new(String::from("Squirtle")))));

        let result = PlayerActionParser::enrich(input_game, input);
        assert_eq!(Ok(expected), result);
    }

    #[test]
    fn should_parse_player_bet_correctly() {
        let input = "Nidoran bets 0.02";
        let mut input_game = Game::new();
        input_game.history.push(Event::new(Phase::Turn, Box::new(PhaseStart::new())));
        let mut expected = Game::new();
        expected.history.push(Event::new(Phase::Turn, Box::new(PhaseStart::new())));
        expected.history.push(Event::new(Phase::Turn, Box::new(Bet::new(String::from("Nidoran"), 0.02, false))));

        let result = PlayerActionParser::enrich(input_game, input);
        assert_eq!(Ok(expected), result);
    }

    #[test]
    fn should_parse_player_bet_all_in_correctly() {
        let input = "Nidoran bets 0.02 and is all-in";
        let mut input_game = Game::new();
        input_game.history.push(Event::new(Phase::Turn, Box::new(PhaseStart::new())));
        let mut expected = Game::new();
        expected.history.push(Event::new(Phase::Turn, Box::new(PhaseStart::new())));
        expected.history.push(Event::new(Phase::Turn, Box::new(Bet::new(String::from("Nidoran"), 0.02, true))));

        let result = PlayerActionParser::enrich(input_game, input);
        assert_eq!(Ok(expected), result);
    }

    #[test]
    fn should_parse_player_call_correctly() {
        let input = "Pikachu calls 1.96";
        let mut input_game = Game::new();
        input_game.history.push(Event::new(Phase::River, Box::new(PhaseStart::new())));
        let mut expected = Game::new();
        expected.history.push(Event::new(Phase::River, Box::new(PhaseStart::new())));
        expected.history.push(Event::new(Phase::River, Box::new(Call::new(String::from("Pikachu"), 1.96, false))));

        let result = PlayerActionParser::enrich(input_game, input);
        assert_eq!(Ok(expected), result);
    }

    #[test]
    fn should_parse_player_call_all_in_correctly() {
        let input = "Pikachu calls 500 and is all-in";
        let mut input_game = Game::new();
        input_game.history.push(Event::new(Phase::River, Box::new(PhaseStart::new())));
        let mut expected = Game::new();
        expected.history.push(Event::new(Phase::River, Box::new(PhaseStart::new())));
        expected.history.push(Event::new(Phase::River, Box::new(Call::new(String::from("Pikachu"), 500.0, true))));

        let result = PlayerActionParser::enrich(input_game, input);
        assert_eq!(Ok(expected), result);
    }
    // Snorlax raises 1.96 to 1.98 and is all-in

    #[test]
    fn should_parse_player_raise_correctly() {
        let input = "Snorlax raises 1.96 to 1.98";
        let mut input_game = Game::new();
        input_game.history.push(Event::new(Phase::PreFlop, Box::new(PhaseStart::new())));
        let mut expected = Game::new();
        expected.history.push(Event::new(Phase::PreFlop, Box::new(PhaseStart::new())));
        expected.history.push(Event::new(Phase::PreFlop, Box::new(Raise::new(String::from("Snorlax"), 1.96, 1.98, false))));

        let result = PlayerActionParser::enrich(input_game, input);
        assert_eq!(Ok(expected), result);
    }

    #[test]
    fn should_parse_player_raise_all_in_correctly() {
        let input = "Snorlax raises 200 to 1000 and is all-in";
        let mut input_game = Game::new();
        input_game.history.push(Event::new(Phase::PreFlop, Box::new(PhaseStart::new())));
        let mut expected = Game::new();
        expected.history.push(Event::new(Phase::PreFlop, Box::new(PhaseStart::new())));
        expected.history.push(Event::new(Phase::PreFlop, Box::new(Raise::new(String::from("Snorlax"), 200.0, 1000.0, true))));

        let result = PlayerActionParser::enrich(input_game, input);
        assert_eq!(Ok(expected), result);
    }


    #[test]
    fn should_return_error_when_parsing_unknown_action() {
        let input = "Snorlax unknown_action 200 to 1000 and is all-in";
        let mut input_game = Game::new();
        input_game.history.push(Event::new(Phase::PreFlop, Box::new(PhaseStart::new())));

        let result = PlayerActionParser::enrich(input_game, input);
        assert_eq!(Err(PlayerActionParseError::InvalidPlayerAction(String::from(input))), result);
    }

    #[test]
    fn should_return_error_when_parsing_player_action_and_no_previous_event_in_game() {
        let input = "Snorlax raises 200 to 1000 and is all-in";
        let mut input_game = Game::new();

        let result = PlayerActionParser::enrich(input_game, input);
        assert_eq!(Err(PlayerActionParseError::UnableToDetermineCurrentGamePhase(String::from(input))), result);
    }
}
