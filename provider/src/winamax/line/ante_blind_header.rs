use std::{error, fmt};

use logos::Logos;

use core::model::game::Game;

use crate::common::Enricher;
use core::model::event::Event;
use core::model::phase::Phase;
use core::model::action::action::Action;
use core::model::action::phase_start::PhaseStart;

pub struct AnteBlindHeader {}

impl Enricher<Game> for AnteBlindHeader {
    type Err = AnteBlindHeaderParseError;

    fn enrich(mut game: Game, line: &str) -> Result<Game, Self::Err> {
        let mut lex = AnteBlindHeaderToken::lexer(line);
        match lex.next() {
            Some(AnteBlindHeaderToken::Header) => {},
            _ => return Err(AnteBlindHeaderParseError::InvalidAnteBlindHeader(String::from(line))),
        }

        let event = Event::new(Phase::AnteBlind, Box::new(PhaseStart::new()));
        game.history.push(event);
        Ok(game)
    }
}

#[derive(Debug, PartialEq)]
pub enum AnteBlindHeaderParseError {
    InvalidAnteBlindHeader(String),
}

impl fmt::Display for AnteBlindHeaderParseError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            AnteBlindHeaderParseError::InvalidAnteBlindHeader(ref s) => write!(f, "Failed to parse ante blind header: '{}'", s),
        }
    }
}

impl error::Error for AnteBlindHeaderParseError {
    fn source(&self) -> Option<&(dyn error::Error + 'static)> {
        None
    }
}


#[derive(Logos, Debug, PartialEq, Clone)]
enum AnteBlindHeaderToken {
    #[token("*** ANTE/BLINDS ***")]
    Header,

    #[error]
    Error,
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn should_parse_ante_blind_header() {
        let input = "*** ANTE/BLINDS ***";
        let mut expected = Game::new();
        expected.history.push(Event::new(Phase::AnteBlind, Box::new(PhaseStart::new())));
        let result = AnteBlindHeader::enrich(Game::new(), input);
        assert_eq!(Ok(expected), result)
    }

    #[test]
    fn should_return_error_on_invalid_header() {
        let input = "*** INVALID_HEADER ***";
        let result = AnteBlindHeader::enrich(Game::new(), input);
        assert_eq!(Err(AnteBlindHeaderParseError::InvalidAnteBlindHeader(String::from(input))), result)
    }

    #[test]
    fn should_parse_ante_blind_header_correctly() {
        let input = "*** ANTE/BLINDS ***";
        let mut lex = AnteBlindHeaderToken::lexer(input);

        // Header
        assert_eq!(Some(AnteBlindHeaderToken::Header), lex.next());
        assert_eq!("*** ANTE/BLINDS ***", lex.slice());
    }
}
