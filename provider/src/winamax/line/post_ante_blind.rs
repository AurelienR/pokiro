use std::{error, fmt};

use logos::{Lexer, Logos};

use core::model::action::post::{Post, PostKind};
use core::model::event::Event;
use core::model::game::Game;
use core::model::phase::Phase;

use crate::common::{Enricher, Parser};
use crate::winamax::model::action::post::PostParseError;
use crate::winamax::parser::WinamaxParser;

pub struct PostAnteBlindParser {}

impl Enricher<Game> for PostAnteBlindParser {
    type Err = PostAnteBlindParseError;

    fn enrich(mut game: Game, line: &str) -> Result<Game, Self::Err> {
        let mut lex = PostAnteBlindToken::lexer(line);

        // Player name + action token
        let mut player_name: String = String::from("");
        let mut post_action_token = PostAnteBlindToken::Error;
        while let Some(token) = lex.next() {
            match token {
                PostAnteBlindToken::PostAction(_) => {
                    post_action_token = token;
                    break;
                }
                _ => player_name.push_str(lex.slice()),
            }
        }
        player_name = player_name.trim().to_string();

        let event = if let PostAnteBlindToken::PostAction(mut post) = post_action_token {
            post.player = player_name;
            Event::new(Phase::AnteBlind, Box::new(post))
        } else {
            return Err(PostAnteBlindParseError::InvalidPostAction(String::from(line)));
        };

        game.history.push(event);
        Ok(game)
    }
}

#[derive(Debug, PartialEq)]
pub enum PostAnteBlindParseError {
    InvalidPostAction(String),
}

impl fmt::Display for PostAnteBlindParseError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            PostAnteBlindParseError::InvalidPostAction(ref s) => write!(f, "Failed to parse post action: '{}'", s),
        }
    }
}

impl error::Error for PostAnteBlindParseError {
    fn source(&self) -> Option<&(dyn error::Error + 'static)> {
        None
    }
}

#[derive(Logos, Debug, PartialEq, Clone)]
pub enum PostAnteBlindToken {
    #[regex(r"posts small blind \d+\.*\d*", PostAnteBlindToken::parse_post_kind)]
    #[regex(r"posts big blind \d+\.*\d*", PostAnteBlindToken::parse_post_kind)]
    #[regex(r"posts ante \d+\.*\d*", PostAnteBlindToken::parse_post_kind)]
    PostAction(Post),

    #[token("out of position")]
    OutOfPosition,

    #[error]
    Error,
}

// Tortoise posts ante 500
// Tortoise posts small blind 0.01
// Tortoise posts big blind 0.02
impl PostAnteBlindToken {
    fn parse_post_kind(lex: &mut Lexer<PostAnteBlindToken>) -> Result<Post, PostParseError> {
        let slice = lex.slice();
        match WinamaxParser::parse(slice) {
            Ok(post) => Ok(post),
            Err(err) => Err(err)
        }
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn should_parse_ante_post_action() {
        let input = "Charmander posts ante 500";
        let mut expected = Game::new();
        expected.history.push(Event::new(
            Phase::AnteBlind,
            Box::new(Post::new(String::from("Charmander"), PostKind::Ante, 500.0)))
        );
        let result = PostAnteBlindParser::enrich(Game::new(), input);
        assert_eq!(Ok(expected), result)
    }

    #[test]
    fn should_parse_small_blind_post_action() {
        let input = "Charmander posts small blind 0.1";
        let mut expected = Game::new();
        expected.history.push(Event::new(
            Phase::AnteBlind,
            Box::new(Post::new(String::from("Charmander"), PostKind::SmallBlind, 0.1)))
        );
        let result = PostAnteBlindParser::enrich(Game::new(), input);
        assert_eq!(Ok(expected), result)
    }

    #[test]
    fn should_parse_big_blind_post_action() {
        let input = "Charmander posts big blind 10";
        let mut expected = Game::new();
        expected.history.push(Event::new(
            Phase::AnteBlind,
            Box::new(Post::new(String::from("Charmander"), PostKind::BigBlind, 10.0)))
        );
        let result = PostAnteBlindParser::enrich(Game::new(), input);
        assert_eq!(Ok(expected), result)
    }

    #[test]
    fn should_return_error_when_parsing_invalid_post_action() {
        let input = "Charmander posts unknown_action 500";
        let result = PostAnteBlindParser::enrich(Game::new(), input);
        assert_eq!(Err(PostAnteBlindParseError::InvalidPostAction(String::from(input))), result)
    }

    #[test]
    fn should_parse_ante() {
        let input = "Tortoise posts ante 500";
        let mut lex = PostAnteBlindToken::lexer(input);

        // Player name
        let mut player_name: String = String::from("");
        let mut post_token = PostAnteBlindToken::Error;
        while let Some(token) = lex.next() {
            match token {
                PostAnteBlindToken::PostAction(_) => {
                    post_token = token;
                    break;
                }
                _ => player_name.push_str(lex.slice()),
            }
        }
        assert_eq!("Tortoise", player_name.trim());

        // PostAction
        assert_eq!(PostAnteBlindToken::PostAction(Post::new(String::from(""),PostKind::Ante, 500.0)), post_token);
    }
}
