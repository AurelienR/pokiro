use std::{error, fmt};

use logos::Logos;

use core::model::action::action::Action;
use core::model::action::phase_start::PhaseStart;
use core::model::event::Event;
use core::model::game::Game;
use core::model::phase::Phase;

use crate::common::Enricher;

pub struct PreFlopHeader {}

impl Enricher<Game> for PreFlopHeader {
    type Err = PreFlopHeaderParseError;

    fn enrich(mut game: Game, line: &str) -> Result<Game, Self::Err> {
        let mut lex = PreFlopHeaderToken::lexer(line);
        match lex.next() {
            Some(PreFlopHeaderToken::Header) => {}
            _ => return Err(PreFlopHeaderParseError::InvalidPreFlopHeader(String::from(line))),
        }

        let event = Event::new(Phase::PreFlop, Box::new(PhaseStart::new()));
        game.history.push(event);
        Ok(game)
    }
}

#[derive(Debug, PartialEq)]
pub enum PreFlopHeaderParseError {
    InvalidPreFlopHeader(String),
}

impl fmt::Display for PreFlopHeaderParseError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            PreFlopHeaderParseError::InvalidPreFlopHeader(ref s) => write!(f, "Failed to parse PreFlop header: '{}'", s),
        }
    }
}

impl error::Error for PreFlopHeaderParseError {
    fn source(&self) -> Option<&(dyn error::Error + 'static)> {
        None
    }
}

#[derive(Logos, Debug, PartialEq, Clone)]
enum PreFlopHeaderToken {
    #[token("*** PRE-FLOP ***")]
    Header,

    #[error]
    Error,
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn should_parse_ante_blind_header() {
        let input = "*** PRE-FLOP ***";
        let mut expected = Game::new();
        expected.history.push(Event::new(Phase::PreFlop, Box::new(PhaseStart::new())));
        let result = PreFlopHeader::enrich(Game::new(), input);
        assert_eq!(Ok(expected), result)
    }

    #[test]
    fn should_return_error_on_invalid_header() {
        let input = "*** INVALID_HEADER ***";
        let result = PreFlopHeader::enrich(Game::new(), input);
        assert_eq!(Err(PreFlopHeaderParseError::InvalidPreFlopHeader(String::from(input))), result)
    }

    #[test]
    fn should_parse_pref_flop_header_correctly() {
        let input = "*** PRE-FLOP ***";
        let mut lex = PreFlopHeaderToken::lexer(input);

        // Header
        assert_eq!(Some(PreFlopHeaderToken::Header), lex.next());
        assert_eq!("*** PRE-FLOP ***", lex.slice());
    }
}
