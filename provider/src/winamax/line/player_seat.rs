use std::num::ParseFloatError;
use std::num::ParseIntError;

use logos::{Lexer, Logos};

use core::model::game::Game;

use crate::common::Enricher;
use std::{fmt, error};
use core::model::player::Player;

pub struct PlayerSeatParser {}

impl Enricher<Game> for PlayerSeatParser {
    type Err = PlayerSeatParseError;

    fn enrich(mut game: Game, line: &str) -> Result<Game, Self::Err> {
        let mut lex = PlayerSeatToken::lexer(line);
        let mut seat_number = 0;
        let mut stack: f32 = 0.0;

        // Seat number
        if let Some(PlayerSeatToken::PlayerSeat(value))= lex.next() {
            seat_number = value;
        } else {
            return Err(PlayerSeatParseError::InvalidPlayerSeatNumber(String::from(line)));
        }

        // Player name + stack
        let mut player_name_buf: String = String::from("");
        let mut stack_token = PlayerSeatToken::Error;
        while let Some(token) = lex.next() {
            match token {
                PlayerSeatToken::Stack(_) => {
                    stack_token = token;
                    break
                },
                _ => player_name_buf.push_str(lex.slice()),
            }
        }

        let player_name = String::from(player_name_buf.trim());

        if let PlayerSeatToken::Stack(value) = stack_token {
            stack = value;
        } else {
            return Err(PlayerSeatParseError::InvalidPlayerStack(String::from(line)));
        }

        let seat_index  = seat_number - 1;

        if seat_index < 0 || (seat_number >= game.seats.len()) {
            return Err(PlayerSeatParseError::PlayerSeatIndexIsOutOfBounds(String::from(line)))
        }

        game.seats[seat_index] = Some(Player::new(player_name, None, stack));
        Ok(game)
    }
}

#[derive(Debug, PartialEq)]
pub enum PlayerSeatParseError {
    InvalidPlayerSeatNumber(String),
    InvalidPlayerStack(String),
    PlayerSeatIndexIsOutOfBounds(String),
}

impl fmt::Display for PlayerSeatParseError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            PlayerSeatParseError::InvalidPlayerSeatNumber(ref s) => write!(f, "Failed to parse player seat number: '{}'", s),
            PlayerSeatParseError::InvalidPlayerStack(ref s) => write!(f, "Failed to parse game player stack: '{}'", s),
            PlayerSeatParseError::PlayerSeatIndexIsOutOfBounds(ref s) => write!(f, "Invalid player seat index is out of bound from game seats: '{}'", s),
        }
    }
}

impl error::Error for PlayerSeatParseError {
    fn source(&self) -> Option<&(dyn error::Error + 'static)> {
        None
    }
}


#[derive(Logos, Debug, PartialEq, Clone)]
enum PlayerSeatToken {
    #[regex(r"Seat \d+:", PlayerSeatToken::parse_seat)]
    PlayerSeat(usize),

    #[token(" ")]
    Whitespace,

    #[regex(r"\(\d+\.*\d*\)", PlayerSeatToken::parse_stack)]
    Stack(f32),

    #[error]
    Error,
}

impl PlayerSeatToken {
    fn parse_stack(lex: &mut Lexer<PlayerSeatToken>) -> Result<f32, ParseFloatError> {
        let slice = lex.slice();
        let cleaned = &slice[1..slice.len() - 1];
        cleaned.parse::<f32>()
    }

    fn parse_seat(lex: &mut Lexer<PlayerSeatToken>) -> Result<usize, ParseIntError> {
        let slice = lex.slice();
        // Remove 'Seat ' and trailing ':'
        slice["Seat ".len()..slice.len() - 1].parse()
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn should_parse_player_seat_correctly() {
        let input = "Seat 3: Beebop-7 (0.81)";
        let mut input_game = Game::new();
        input_game.seats = vec![None; 6];
        let mut expected = Game::new();
        expected.seats = vec![None; 6];
        expected.seats[2] = Some(Player::new(String::from("Beebop-7"), None, 0.81));

        let result = PlayerSeatParser::enrich(input_game, input);
        assert_eq!(Ok(expected), result);
    }

    #[test]
    fn should_return_error_on_wrong_player_seat_number() {
        let input = "Seat A: Beebop-7 (0.81)";
        let result = PlayerSeatParser::enrich(Game::new(), input);
        assert_eq!(Err(PlayerSeatParseError::InvalidPlayerSeatNumber(String::from(input))), result);
    }

    #[test]
    fn should_return_error_on_wrong_player_stack() {
        let input = "Seat 3: Beebop-7 (-0.58)";
        let result = PlayerSeatParser::enrich(Game::new(), input);
        assert_eq!(Err(PlayerSeatParseError::InvalidPlayerStack(String::from(input))), result);
    }

    #[test]
    fn should_return_error_on_wrong_out_of_bound_game_seats() {
        let input = "Seat 6: Beebop-7 (0.81)";
        let mut input_game = Game::new();
        input_game.seats = vec![None;5];
        let result = PlayerSeatParser::enrich(input_game, input);
        assert_eq!(Err(PlayerSeatParseError::PlayerSeatIndexIsOutOfBounds(String::from(input))), result);
    }


    #[test]
    fn should_parse_with_player_name_with_dash_and_number_line() {
        let input = "Seat 1: Walou-7 (0.81)";
        let mut lex = PlayerSeatToken::lexer(input);

        // Seat number
        assert_eq!(Some(PlayerSeatToken::PlayerSeat(1)), lex.next());
        assert_eq!("Seat 1:", lex.slice());

        // Player name
        let mut buffer: String = String::from("");
        let mut stack_token = PlayerSeatToken::Error;
        while let Some(token) = lex.next() {
            match token {
                PlayerSeatToken::Stack(_) => {
                    stack_token = token;
                    break
                },
                _ => buffer.push_str(lex.slice()),
            }
        }
        assert_eq!("Walou-7", buffer.trim());

        // Stack
        assert_eq!(PlayerSeatToken::Stack(0.81), stack_token);
    }

    #[test]
    fn should_parse_with_player_name_with_whitespace() {
        let input = "Seat 10: Pidgeot (10.01)";
        let mut lex = PlayerSeatToken::lexer(input);

        // Seat number
        assert_eq!(Some(PlayerSeatToken::PlayerSeat(10)), lex.next());
        assert_eq!("Seat 10:", lex.slice());

        // Player name
        let mut buffer: String = String::from("");
        let mut stack_token = PlayerSeatToken::Error;
        while let Some(token) = lex.next() {
            match token {
                PlayerSeatToken::Stack(_) => {
                    stack_token = token;
                    break
                },
                _ => buffer.push_str(lex.slice()),
            }
        }
        assert_eq!("Pidgeot", buffer.trim());

        // Stack
        assert_eq!(PlayerSeatToken::Stack(10.01), stack_token);
    }
}
