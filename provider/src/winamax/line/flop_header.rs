use std::{error, fmt};

use logos::{Lexer, Logos};

use core::model::board::{Board, Flop};
use core::model::game::Game;

use crate::common::{Enricher, Parser};
use crate::winamax::model::board::FlopParseError;
use crate::winamax::parser::WinamaxParser;

pub struct FlopHeader {}

impl Enricher<Game> for FlopHeader {
    type Err = FlopHeaderParseError;

    fn enrich(mut game: Game, line: &str) -> Result<Game, Self::Err> {
        let mut lex = FlopHeaderToken::lexer(line);

        let flop = match lex.next() {
            Some(FlopHeaderToken::Header(flop)) => flop,
            _ => return Err(FlopHeaderParseError::InvalidFlopHeader(String::from(line)))
        };

        game.board.flop = Some(flop);
        Ok(game)
    }
}

#[derive(Debug, PartialEq)]
pub enum FlopHeaderParseError {
    InvalidFlopHeader(String),
}

impl fmt::Display for FlopHeaderParseError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            FlopHeaderParseError::InvalidFlopHeader(ref s) => write!(f, "Failed to parse Flop header: '{}'", s),
        }
    }
}

impl error::Error for FlopHeaderParseError {
    fn source(&self) -> Option<&(dyn error::Error + 'static)> {
        None
    }
}

// *** FLOP *** [7c 6c 4s]
#[derive(Logos, Debug, PartialEq, Clone)]
enum FlopHeaderToken {
    #[regex(r"\*\*\* FLOP \*\*\* \[\w\w \w\w \w\w\]", FlopHeaderToken::strip_header_and_parse)]
    Header(Flop),

    #[error]
    Error,
}

impl FlopHeaderToken {
    fn strip_header_and_parse(lex: &mut Lexer<FlopHeaderToken>) -> Result<Flop, FlopParseError> {
        let slice = lex.slice();
        let card = slice.trim_start_matches("*** FLOP *** [")
            .trim_end_matches("]");
        WinamaxParser::parse(card)
    }
}

#[cfg(test)]
mod test {
    use super::*;
    use core::model::card::card::Card;
    use core::model::card::rank::Rank;
    use core::model::card::suit::Suit;

    #[test]
    fn should_parse_flop_header() {
        let input = "*** FLOP *** [Ac 6c 4s]";
        let mut expected = Game::new();
        expected.board.flop = Some((
            Card::new(Rank::Ace, Suit::Club),
            Card::new(Rank::Six, Suit::Club),
            Card::new(Rank::Four, Suit::Spade)
        ));
        let result = FlopHeader::enrich(Game::new(), input);
        assert_eq!(Ok(expected), result)
    }

    #[test]
    fn should_return_error_on_invalid_header() {
        let input = "*** INVALID_HEADER ***";
        let result = FlopHeader::enrich(Game::new(), input);
        assert_eq!(Err(FlopHeaderParseError::InvalidFlopHeader(String::from(input))), result)
    }

    #[test]
    fn should_return_error_when_flop_header_contains_invalid_card() {
        let input = "*** FLOP *** [7c 11c 4s]";
        let result = FlopHeader::enrich(Game::new(), input);
        assert_eq!(Err(FlopHeaderParseError::InvalidFlopHeader(String::from(input))), result);
    }
}
