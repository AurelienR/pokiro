use core::model::game::Game;


pub struct GameStateMatchine {
    state: GameState,
    game: Game,
    // TODO: Use a callback ?
    on_game_parsed: fn(Game) -> (),
}

impl GameStateMatchine {
    pub fn new<F>(on_game_parsed: fn(Game) -> ()) -> Self {
        Self {
            state: GameState::WAITING_FOR_NEW_GAME,
            game: Game::new(),
            on_game_parsed,
        }
    }

    pub fn next(self: Self, line: &String) -> Result<Self, String> {
        todo!("
        1. Maybe extract this in a trait\n
        2. Implement different state handling
        ");
        Ok(self)
    }
}

pub enum GameState {
    // Start
    WAITING_FOR_NEW_GAME,

    // Game setup
    NEW_GAME_STARTED,
    TABLE_OK,
    SEAT_OK,
    ANTE_BLIND_STARTED,
    ANTE_OK,
    SMALL_BLIND_OK,
    HERO_DEALT_OK,

    // Game started
    PRE_FLOP_STARTED,
    TURN_STARTED,
    RIVER_STARTED,
    PLAYER_ACTION_DONE,

    // Game resolved
    SHOW_DOWN_STARTED,
    PLAYER_SHOWED,
    PLAYER_REWARDED,

    // Summary
    SUMMARY,
    SUM_TOTAL_RAKE_OK,
    SUM_BOARD_OK,
    SUM_PLAYER_REWARD_OK,

    // Invalid parse or transition
    ERROR,
}
