use std::{error, fmt};

use core::model::board::{Flop, Turn, River};
use core::model::card::card::Card;

use crate::common::Parser;
use crate::winamax::model::card::CardParseError;
use crate::winamax::parser::WinamaxParser;

const PREFIX: &str = "[";
const SUFFIX: &str = "]";

impl Parser<Flop> for WinamaxParser {
    type Err = FlopParseError;

    fn parse(s: &str) -> Result<Flop, Self::Err> {
        let cleaned = s.trim_start_matches(PREFIX)
            .trim_end_matches(SUFFIX);
        let hands_str: Vec<&str> = cleaned.split(' ').collect();
        match (WinamaxParser::parse(hands_str[0]), WinamaxParser::parse(hands_str[1]), WinamaxParser::parse(hands_str[2])) {
            (Ok(card1), Ok(card2), Ok(card3)) => Ok((card1, card2, card3)),
            (Err(err), _, _) => Err(FlopParseError::Card1(String::from(s), err)),
            (_, Err(err), _) => Err(FlopParseError::Card2(String::from(s), err)),
            (_, _, Err(err)) => Err(FlopParseError::Card3(String::from(s), err)),
        }
    }
}


#[derive(Debug, PartialEq)]
pub enum FlopParseError {
    Card1(String, CardParseError),
    Card2(String, CardParseError),
    Card3(String, CardParseError),
}

impl fmt::Display for FlopParseError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            FlopParseError::Card1(ref s, ref cause) => write!(
                f,
                "Error parsing while parsing first Card from Flop - input: '{}', reason: {}",
                s, cause
            ),
            FlopParseError::Card2(ref s, ref cause) => write!(
                f,
                "Error parsing while parsing second Card from Flop - input: '{}', reason: {}",
                s, cause
            ),
            FlopParseError::Card3(ref s, ref cause) => write!(
                f,
                "Error parsing while parsing third Card from Flop - input: '{}', reason: {}",
                s, cause
            )
        }
    }
}

impl error::Error for FlopParseError {
    fn source(&self) -> Option<&(dyn error::Error + 'static)> {
        match *self {
            FlopParseError::Card1(_, ref cause) => Some(cause),
            FlopParseError::Card2(_, ref cause) => Some(cause),
            FlopParseError::Card3(_, ref cause) => Some(cause),
        }
    }
}


#[cfg(test)]
mod tests {
    use core::model::card::card::Card;
    use core::model::card::rank::Rank;
    use core::model::card::suit::Suit;

    use super::*;

    #[test]
    fn should_parse_flop_correctly() {
        let result = WinamaxParser::parse("[8h Ts Kd]");
        let expected : Flop  = (
            Card::new(Rank::Eight, Suit::Heart),
            Card::new(Rank::Ten, Suit::Spade),
            Card::new(Rank::King, Suit::Diamond)
        );
        assert_eq!(Ok(expected), result)
    }

    #[test]
    fn should_return_error_when_failing_parsing_first_card_in_flop() {
        let input = "[8y Ts Kd]";
        let result = <WinamaxParser as Parser<Flop>>::parse(input);
        match result {
            Err(FlopParseError::Card1(ref s, _)) => assert_eq!(input, s),
            _ => assert!(false)
        }
    }

    #[test]
    fn should_return_error_when_failing_parsing_second_card_in_flop() {
        let input = "[8h 11s Kd]";
        let result = <WinamaxParser as Parser<Flop>>::parse(input);
        match result {
            Err(FlopParseError::Card2(ref s, _)) => assert_eq!(input, s),
            _ => assert!(false)
        }
    }

    #[test]
    fn should_return_error_when_failing_parsing_third_card_in_flop() {
        let input = "[8h Ts K]";
        let result = <WinamaxParser as Parser<Flop>>::parse(input);
        match result {
            Err(FlopParseError::Card3(ref s, _)) => assert_eq!(input, s),
            _ => assert!(false)
        }
    }
}
