use std::error;
use std::fmt;
use std::result::Result;

use core::model::card::card::Card;

use crate::common::Parser;

use super::rank::RankParseError;
use super::suit::SuitParseError;
use crate::winamax::parser::WinamaxParser;

impl Parser<Card> for WinamaxParser {
    type Err = CardParseError;

    fn parse(s: &str) -> Result<Card, Self::Err> {
        let mut chars = s.chars();
        match (chars.next(), chars.next()) {
            (Some(rank_str), Some(suit_str)) => {
                match (
                    WinamaxParser::parse(&rank_str.to_string()),
                    WinamaxParser::parse(&suit_str.to_string()),
                ) {
                    (Ok(rank), Ok(suit)) => Ok(Card::new(rank, suit)),
                    (Err(err), _) => Err(CardParseError::Rank(String::from(s), err)),
                    (_, Err(err)) => Err(CardParseError::Suit(String::from(s), err)),
                }
            }
            _ => Err(CardParseError::NotEnoughCharacters(String::from(s))),
        }
    }
}


#[derive(Debug, PartialEq)]
pub enum CardParseError {
    Rank(String, RankParseError),
    Suit(String, SuitParseError),
    NotEnoughCharacters(String),
}

impl fmt::Display for CardParseError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            CardParseError::Rank(ref s, ref cause) => write!(
                f,
                "Error parsing while Card - unable to parse Rank from string '{}', reason: {}",
                s, cause
            ),
            CardParseError::Suit(ref s, ref cause) => write!(
                f,
                "Error parsing while Card - unable to parse Suit from string '{}', reason: {}",
                s, cause
            ),
            CardParseError::NotEnoughCharacters(ref s) => write!(
                f,
                "Error parsing while Card not enougth character from string '{}'",
                s
            ),
        }
    }
}

impl error::Error for CardParseError {
    fn source(&self) -> Option<&(dyn error::Error + 'static)> {
        match *self {
            CardParseError::Rank(_, ref cause) => Some(cause),
            CardParseError::Suit(_, ref cause) => Some(cause),
            CardParseError::NotEnoughCharacters(_) => None,
        }
    }
}

#[cfg(test)]
mod test {
    use core::model::card::rank::Rank;
    use core::model::card::suit::Suit;

    use super::*;

    #[test]
    fn should_parse_nine_of_spade() {
        let result = WinamaxParser::parse("9s");
        assert_eq!(Ok(Card::new(Rank::Nine, Suit::Spade)), result)
    }

    #[test]
    fn should_parse_ace_of_diamond() {
        let result = WinamaxParser::parse("Ad");
        assert_eq!(Ok(Card::new(Rank::Ace, Suit::Diamond)), result)
    }

    #[test]
    fn should_parse_king_of_heart() {
        let result = WinamaxParser::parse("Kh");
        assert_eq!(Ok(Card::new(Rank::King, Suit::Heart)), result)
    }

    #[test]
    fn should_parse_jack_of_club() {
        let result = WinamaxParser::parse("Jc");
        assert_eq!(Ok(Card::new(Rank::Jack, Suit::Club)), result)
    }

    #[test]
    fn should_return_error_when_parsing_card_with_unknown_rank() {
        let input = "Vs";
        match <WinamaxParser as Parser<Card>>::parse(input) {
            Err(CardParseError::Rank(ref s, _)) => assert_eq!(input, s),
            _ => assert!(false),
        }
    }

    #[test]
    fn should_return_error_when_parsing_card_with_unknown_suit() {
        let input = "8p";
        match <WinamaxParser as Parser<Card>>::parse(input) {
            Err(CardParseError::Suit(ref s, _)) => assert_eq!(input, s),
            _ => assert!(false),
        }
    }

    #[test]
    fn should_return_error_when_parsing_card_with_not_enough_chars() {
        let input = "8";
        match <WinamaxParser as Parser<Card>>::parse(input) {
            Err(CardParseError::NotEnoughCharacters(ref s)) => assert_eq!(input, s),
            _ => assert!(false),
        }
    }
}
