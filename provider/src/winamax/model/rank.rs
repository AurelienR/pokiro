use std::error;
use std::fmt;

use core::model::card::rank::Rank;

use crate::common::Parser;
use crate::winamax::parser::WinamaxParser;


impl Parser<Rank> for WinamaxParser {
    type Err = RankParseError;

    fn parse(s: &str) -> Result<Rank, Self::Err> {
        match s {
            "A" => Ok(Rank::Ace),
            "K" => Ok(Rank::King),
            "Q" => Ok(Rank::Queen),
            "J" => Ok(Rank::Jack),
            "T" => Ok(Rank::Ten),
            "9" => Ok(Rank::Nine),
            "8" => Ok(Rank::Eight),
            "7" => Ok(Rank::Seven),
            "6" => Ok(Rank::Six),
            "5" => Ok(Rank::Five),
            "4" => Ok(Rank::Four),
            "3" => Ok(Rank::Three),
            "2" => Ok(Rank::Two),
            _ => Err(RankParseError::UnknownRank(String::from(s)))
        }
    }
}

#[derive(Debug, PartialEq)]
pub enum RankParseError {
    UnknownRank(String),
}

impl fmt::Display for RankParseError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            RankParseError::UnknownRank(ref s) => {
                write!(f, "Error parsing Rank from string '{}'", s)
            }
        }
    }
}

impl error::Error for RankParseError {
    fn source(&self) -> Option<&(dyn error::Error + 'static)> {
        None
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn should_parse_ace() {
        assert_eq!(Ok(Rank::Ace), WinamaxParser::parse("A"));
    }

    #[test]
    fn should_parse_king() {
        assert_eq!(Ok(Rank::King), WinamaxParser::parse("K"));
    }

    #[test]
    fn should_parse_queen() {
        assert_eq!(Ok(Rank::Queen), WinamaxParser::parse("Q"));
    }

    #[test]
    fn should_parse_jack() {
        assert_eq!(Ok(Rank::Jack), WinamaxParser::parse("J"));
    }

    #[test]
    fn should_parse_ten() {
        assert_eq!(Ok(Rank::Ten), WinamaxParser::parse("T"));
    }

    #[test]
    fn should_parse_nine() {
        assert_eq!(Ok(Rank::Nine), WinamaxParser::parse("9"));
    }

    #[test]
    fn should_parse_eight() {
        assert_eq!(Ok(Rank::Eight), WinamaxParser::parse("8"));
    }

    #[test]
    fn should_parse_seven() {
        assert_eq!(Ok(Rank::Seven), WinamaxParser::parse("7"));
    }

    #[test]
    fn should_parse_six() {
        assert_eq!(Ok(Rank::Six), WinamaxParser::parse("6"));
    }

    #[test]
    fn should_parse_five() {
        assert_eq!(Ok(Rank::Five), WinamaxParser::parse("5"));
    }

    #[test]
    fn should_parse_four() {
        assert_eq!(Ok(Rank::Four), WinamaxParser::parse("4"));
    }

    #[test]
    fn should_parse_three() {
        assert_eq!(Ok(Rank::Three), WinamaxParser::parse("3"));
    }

    #[test]
    fn should_parse_two() {
        assert_eq!(Ok(Rank::Two), WinamaxParser::parse("2"));
    }

    #[test]
    fn should_return_error_when_parsing_unknown_rank() {
        let input = "unknown_rank";
        match <WinamaxParser as Parser<Rank>>::parse(input) {
            Err(RankParseError::UnknownRank(ref s)) => assert_eq!(input, s),
            _ => assert!(false),
        }
    }
}
