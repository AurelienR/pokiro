use std::error;
use std::fmt;
use std::num::ParseFloatError;
use std::result::Result;

use core::model::action::post::{Post, PostKind};


use crate::common::Parser;
use crate::winamax::parser::WinamaxParser;

// Keywords
const POSTS: &str = "posts";

// Should be able to parse
//      - 'posts ante 500'
//      - 'posts small blind 0.02'
//      - 'posts big blind 0.02'
// TODO: Take into account all-in on posting blind?
impl Parser<Post> for WinamaxParser {
    type Err = PostParseError;

    fn parse(s: &str) -> Result<Post, Self::Err> {
        let input = s.trim();
        let v: Vec<&str> = input.trim_start_matches(POSTS).trim().split(" ").collect();
        // Concatenate 'big'/'small' with 'blind'
        let kind_str = v[0..v.len() - 1].join(" ");
        let amount = v[v.len() - 1];
        match (WinamaxParser::parse(&kind_str), amount.parse::<f32>()) {
            (Ok(kind), Ok(amount)) => Ok(Post::new(String::from(""), kind, amount)),
            (Result::Err(cause), _) => Err(PostParseError::Kind(String::from(s), cause)),
            (_, Result::Err(cause)) => Err(PostParseError::Amount(String::from(s), cause)),
        }
    }
}

#[derive(Debug, PartialEq)]
pub enum PostParseError {
    Kind(String, PostKindParseError),
    Amount(String, ParseFloatError),
}

impl fmt::Display for PostParseError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            PostParseError::Kind(ref s, ref cause) => write!(
                f,
                "Error parsing while Post action - unable to parse Kind from string '{}', reason: {}",
                s, cause
            ),
            PostParseError::Amount(ref s, ref cause) => write!(
                f,
                "Error parsing while Post action - unable to parse Amount from string '{}', reason: {}",
                s, cause
            ),
        }
    }
}

impl error::Error for PostParseError {
    fn source(&self) -> Option<&(dyn error::Error + 'static)> {
        match *self {
            PostParseError::Kind(_, ref cause) => Some(cause),
            PostParseError::Amount(_, ref cause) => Some(cause),
        }
    }
}



impl Parser<PostKind> for WinamaxParser {
    type Err = PostKindParseError;

    fn parse(s: &str) -> Result<PostKind, Self::Err> {
        match s.trim() {
            "ante" => Ok(PostKind::Ante),
            "small blind" => Ok(PostKind::SmallBlind),
            "big blind" => Ok(PostKind::BigBlind),
            s => Err(PostKindParseError::UnknownKind(String::from(s))),
        }
    }
}

#[derive(Debug, PartialEq)]
pub enum PostKindParseError {
    UnknownKind(String),
}

impl fmt::Display for PostKindParseError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            PostKindParseError::UnknownKind(ref s) => {
                write!(f, "Error parsing Kind from string '{}', unknown kind", s)
            }
        }
    }
}

impl error::Error for PostKindParseError {
    fn source(&self) -> Option<&(dyn error::Error + 'static)> {
        None
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn should_parse_ante_post() {
        let input = " posts ante 500 ";
        let expected = Post::new(String::from(""), PostKind::Ante, 500.00);
        let result = WinamaxParser::parse(input);
        assert_eq!(Ok(expected), result);
    }

    #[test]
    fn should_parse_small_blind_post() {
        let input = " posts small blind 0.02 ";
        let expected = Post::new(String::from(""),PostKind::SmallBlind, 0.02);
        let result = WinamaxParser::parse(input);
        assert_eq!(Ok(expected), result);
    }

    #[test]
    fn should_parse_big_blind_post() {
        let input = " posts big blind 10.0 ";
        let expected = Post::new(String::from(""), PostKind::BigBlind, 10.0);
        let result = WinamaxParser::parse(input);
        assert_eq!(Ok(expected), result);
    }

    #[test]
    fn should_return_error_when_parsing_post_unknown_kind() {
        let input = " unknown_kind 10.0 ";
        let expected = PostParseError::Kind(
            String::from(input),
            PostKindParseError::UnknownKind(String::from("unknown_kind"))
        );
        let result = <WinamaxParser as Parser<Post>>::parse(input);
        assert_eq!(Err(expected), result);
    }

    #[test]
    fn should_return_error_when_parsing_post_unknown_amount() {
        let input = " posts big blind unknown_amount ";
        let result = <WinamaxParser as Parser<Post>>::parse(input);

        assert!(result.is_err());
        if let Err(PostParseError::Amount(s, _)) = result {
            assert_eq!(String::from(" posts big blind unknown_amount "), s);
        } else {
            assert!(false)
        }
    }


    #[test]
    fn should_parse_ante_keyword() {
        let input = "ante ";
        let expected = PostKind::Ante;
        let result = WinamaxParser::parse(input);
        assert_eq!(Ok(expected), result);
    }

    #[test]
    fn should_parse_small_blind_keyword() {
        let input = " small blind ";
        let expected = PostKind::SmallBlind;
        let result = WinamaxParser::parse(input);
        assert_eq!(Ok(expected), result);
    }

    #[test]
    fn should_parse_big_blind_keyword() {
        let input = " big blind ";
        let expected = PostKind::BigBlind;
        let result = WinamaxParser::parse(input);
        assert_eq!(Ok(expected), result);
    }

    #[test]
    fn should_return_error_when_parsing_post_kind_unknown_kind() {
        let input = " unknown_kind ";
        let expected = PostKindParseError::UnknownKind(String::from("unknown_kind"));
        let result = <WinamaxParser as Parser<PostKind>>::parse(input);
        assert_eq!(Err(expected), result);
    }
}


