use std::error;
use std::fmt;
use std::result::Result;

use core::model::action::check::Check;

use crate::common::Parser;
use crate::winamax::parser::WinamaxParser;


// Keywords
const CHECKS: &str = "checks";

impl Parser<Check> for WinamaxParser {
    type Err = CheckParseError;

    fn parse(s: &str) -> Result<Check, Self::Err> {
        match s.trim() {
            CHECKS => Ok(Check::default()),
            _ => Err(CheckParseError::UnknownCheck(String::from(s))),
        }
    }
}

#[derive(Debug, PartialEq)]
pub enum CheckParseError {
    UnknownCheck(String),
}

impl fmt::Display for CheckParseError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            CheckParseError::UnknownCheck(ref s) => {
                write!(f, "Error parsing Check Action from string '{}'", s)
            }
        }
    }
}

impl error::Error for CheckParseError {
    fn source(&self) -> Option<&(dyn error::Error + 'static)> {
        None
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn should_parse_check_keyword() {
        let input = "checks ";
        let expected = Check::default();
        let result = WinamaxParser::parse(input);
        assert_eq!(Ok(expected), result);
    }


    #[test]
    fn should_return_error_when_parsing_unknown_checks() {
        let input = "unknown_check";
        let expected = CheckParseError::UnknownCheck(String::from(input));
        let result = <WinamaxParser as Parser<Check>>::parse(input);
        assert_eq!(Err(expected), result);
    }
}
