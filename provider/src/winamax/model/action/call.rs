use std::error;
use std::fmt;
use std::num::ParseFloatError;
use std::result::Result;

use core::model::action::call::Call;

use crate::common::Parser;

use crate::winamax::parser::WinamaxParser;

// Keywords
const CALLS: &str = "calls";
const ALL_IN: &str = "and is all-in";

// Be able to parse 'calls 1.96 and is all-in'
impl Parser<Call> for WinamaxParser {
    type Err = CallParseError;

    fn parse(s: &str) -> Result<Call, Self::Err> {
        let input = s.trim();
        let cleaned = s.trim_start_matches(CALLS)
            .trim_end_matches(ALL_IN)
            .trim();
        let all_in = input.ends_with(ALL_IN);

        match cleaned.parse::<f32>() {
            Ok(amount) => {
                let mut call = Call::default();
                call.amount = amount;
                call.all_in = all_in;
                Ok(call)
            },
            Result::Err(err) => Err(CallParseError::Amount(String::from(s), err)),
        }
    }
}

#[derive(Debug, PartialEq)]
pub enum CallParseError {
    Amount(String, ParseFloatError),
}

impl fmt::Display for CallParseError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            CallParseError::Amount(ref s, ref cause) => write!(
                f,
                "Error parsing while Call action -  unable to parse Amount from string '{}', reason: {}",
                s, cause
            )
        }
    }
}

impl error::Error for CallParseError {
    fn source(&self) -> Option<&(dyn error::Error + 'static)> {
        match *self {
            CallParseError::Amount(_, ref cause) => Some(cause),
        }
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn should_parse_call_float_amount() {
        let input = "calls 2.85";
        let expected = Call::new(String::from(""),  2.85, false);
        let result = WinamaxParser::parse(input);
        assert_eq!(Ok(expected), result);
    }

    #[test]
    fn should_parse_call_int_amount() {
        let input = "calls 2";
        let expected = Call::new(String::from(""),2.00, false);
        let result = WinamaxParser::parse(input);
        assert_eq!(Ok(expected), result);
    }

    #[test]
    fn should_parse_call_with_all_in() {
        let input = "calls 2.85 and is all-in";
        let expected = Call::new(String::from(""),2.85, true);
        let result = WinamaxParser::parse(input);
        assert_eq!(Ok(expected), result);
    }

    #[test]
    fn should_return_error_when_parsing_call_unknown_amount() {
        let input = "calls unknown_amount and is all-in";
        let result = <WinamaxParser as Parser<Call>>::parse(input);
        assert!(result.is_err());
        if let Err(CallParseError::Amount(s, _)) = result {
            assert_eq!(String::from(input), s);
        }
    }
}
