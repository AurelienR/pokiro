use std::error;
use std::fmt;
use std::num::ParseFloatError;
use std::result::Result;

use core::model::action::raise::Raise;

use crate::common::Parser;

use crate::winamax::parser::WinamaxParser;

// Keywords
const RAISES: &str = "raises";
const TO: &str = "to";
const ALL_IN: &str = "and is all-in";

// Be able to parse 'raises 0.02 to 0.04 and is all-in'
impl Parser<Raise> for WinamaxParser {
    type Err = RaiseParseError;

    fn parse(s: &str) -> Result<Raise, Self::Err> {
        let input = s.trim();
        let v: Vec<Result<f32, ParseFloatError>> = input
            .trim_start_matches(RAISES)
            .trim_end_matches(ALL_IN)
            .trim()
            .split(TO)
            .map(|value| value.trim())
            .map(|value| value.parse::<f32>())
            .collect();
        let all_in = input.ends_with(ALL_IN);
        match (&v[0], &v[1]) {
            (Ok(from), Ok(to)) => Ok(Raise::new(String::from("") ,*from, *to, all_in)),
            (Result::Err(err), _) => Err(RaiseParseError::From(String::from(s), err.clone())),
            (_, Result::Err(err)) => Err(RaiseParseError::To(String::from(s), err.clone())),
        }
    }
}

#[derive(Debug, PartialEq)]
pub enum RaiseParseError {
    From(String, ParseFloatError),
    To(String, ParseFloatError),
}

impl fmt::Display for RaiseParseError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            RaiseParseError::From(ref s, ref cause) => write!(
                f,
                "Error parsing while Raise action -  unable to parse From amount from string '{}', reason: {}",
                s, cause
            ),
            RaiseParseError::To(ref s, ref cause) => write!(
                f,
                "Error parsing while Raise action -  unable to parse To amount from string '{}', reason: {}",
                s, cause
            ),
        }
    }
}

impl error::Error for RaiseParseError {
    fn source(&self) -> Option<&(dyn error::Error + 'static)> {
        match *self {
            RaiseParseError::From(_, ref cause) => Some(cause),
            RaiseParseError::To(_, ref cause) => Some(cause),
        }
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn should_parse_raise_without_all_in() {
        let input = " raises 1.96 to 1.98 ";
        let expected = Raise::new(String::from(""),1.96, 1.98, false);
        let result = WinamaxParser::parse(input);
        assert_eq!(Ok(expected), result);
    }

    #[test]
    fn should_parse_raise_with_all_in() {
        let input = " raises 100 to 200 and is all-in ";
        let expected = Raise::new(String::from(""), 100.00, 200.00, true);
        let result = WinamaxParser::parse(input);
        assert_eq!(Ok(expected), result);
    }

    #[test]
    fn should_return_error_when_parsing_raise_unknown_from_amount() {
        let input = " raises unknown_amount to 1.98 ";
        let result = <WinamaxParser as Parser<Raise>>::parse(input);
        assert!(result.is_err());
        if let Err(RaiseParseError::From(s, _)) = result {
            assert_eq!(String::from(input), s);
        } else { assert!(false) }
    }

    #[test]
    fn should_return_error_when_parsing_raise_unknown_to_amount() {
        let input = " raises 1.98 to unknown_amount ";
        let result = <WinamaxParser as Parser<Raise>>::parse(input);
        assert!(result.is_err());
        if let Err(RaiseParseError::To(s, _)) = result {
            assert_eq!(String::from(input), s);
        } else { assert!(false) }
    }
}
