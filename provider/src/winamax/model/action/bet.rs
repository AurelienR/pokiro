use std::error;
use std::fmt;
use std::num::ParseFloatError;
use std::result::Result;

use core::model::action::bet::Bet;

use crate::common::Parser;

use crate::winamax::parser::WinamaxParser;

// Keywords
const BETS: &str = "bets";
const ALL_IN: &str = "and is all-in";

// Be able to parse 'bets 2.85 and is all-in'
impl Parser<Bet> for WinamaxParser {
    type Err = BetParseError;

    fn parse(s: &str) -> Result<Bet, Self::Err> {
        let input = s.trim();
        let cleaned = s.trim_start_matches(BETS)
            .trim_end_matches(ALL_IN)
            .trim();
        let all_in = input.ends_with(ALL_IN);

        match cleaned.parse::<f32>() {
            Ok(amount) => Ok(Bet::new(String::from(""),amount, all_in)),
            Result::Err(err) => Err(BetParseError::Amount(String::from(s), err)),
        }
    }
}

#[derive(Debug, PartialEq)]
pub enum BetParseError {
    Amount(String, ParseFloatError),
}

impl fmt::Display for BetParseError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            BetParseError::Amount(ref s, ref cause) => write!(
                f,
                "Error parsing while Bet action -  unable to parse Amount from string '{}', reason: {}",
                s, cause
            )
        }
    }
}

impl error::Error for BetParseError {
    fn source(&self) -> Option<&(dyn error::Error + 'static)> {
        match *self {
            BetParseError::Amount(_, ref cause) => Some(cause),
        }
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn should_parse_bet_float_amount() {
        let input = "bets 2.85";
        let expected = Bet::new(String::from(""),2.85, false);
        let result = WinamaxParser::parse(input);
        assert_eq!(Ok(expected), result);
    }

    #[test]
    fn should_parse_bet_int_amount() {
        let input = "bets 2";
        let expected = Bet::new(String::from(""),2.00, false);
        let result = WinamaxParser::parse(input);
        assert_eq!(Ok(expected), result);
    }

    #[test]
    fn should_parse_bet_with_all_in() {
        let input = "bets 2.85 and is all-in";
        let expected = Bet::new(String::from(""), 2.85, true);
        let result = WinamaxParser::parse(input);
        assert_eq!(Ok(expected), result);
    }

    #[test]
    fn should_return_error_when_parsing_bet_unknown_amount() {
        let input = "bets unknown_amount and is all-in";
        let result = <WinamaxParser as Parser<Bet>>::parse(input);
        assert!(result.is_err());
        if let Err(BetParseError::Amount(s, _)) = result {
            assert_eq!(String::from(input), s);
        }
    }
}
