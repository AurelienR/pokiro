use std::error;
use std::fmt;
use std::result::Result;

use core::model::action::fold::Fold;

use crate::common::Parser;
use crate::winamax::parser::WinamaxParser;


// Keywords
const FOLDS: &str = "folds";

// Be able to parse 'folds'
impl Parser<Fold> for WinamaxParser {
    type Err = FoldParseError;

    fn parse(s: &str) -> Result<Fold, Self::Err> {
        match s.trim() {
            FOLDS => Ok(Fold::default()),
            _ => Err(FoldParseError::UnknownFold(String::from(s))),
        }
    }
}

#[derive(Debug, PartialEq)]
pub enum FoldParseError {
    UnknownFold(String),
}

impl fmt::Display for FoldParseError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            FoldParseError::UnknownFold(ref s) => {
                write!(f, "Error parsing Fold Action from string '{}'", s)
            }
        }
    }
}

impl error::Error for FoldParseError {
    fn source(&self) -> Option<&(dyn error::Error + 'static)> {
        None
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn should_parse_fold_keyword() {
        let input = "folds ";
        let expected = Fold::default();
        let result = WinamaxParser::parse(input);
        assert_eq!(Ok(expected), result);
    }

    #[test]
    fn should_return_error_when_parsing_unknown_fold() {
        let input = "unknown_fold";
        let expected = FoldParseError::UnknownFold(String::from(input));
        let result = <WinamaxParser as Parser<Fold>>::parse(input);
        assert_eq!(Err(expected), result);
    }
}
