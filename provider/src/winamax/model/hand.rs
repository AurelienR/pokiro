use std::error;
use std::fmt;
use std::result::Result;

use core::model::hand::Hand;

use crate::common::Parser;

use super::card::CardParseError;
use crate::winamax::parser::WinamaxParser;

const HAND_PREFIX: &str = "[";
const HAND_SUFFIX: &str = "]";

impl Parser<Hand> for WinamaxParser {
    type Err = HandParseError;

    fn parse(s: &str) -> Result<Hand, Self::Err> {
        let cleaned = s.trim_start_matches(HAND_PREFIX)
            .trim_end_matches(HAND_SUFFIX);
        let hands_str: Vec<&str> = cleaned.split(' ').collect();
        match (WinamaxParser::parse(hands_str[0]), WinamaxParser::parse(hands_str[1])) {
            (Ok(card1), Ok(card2)) => Ok(Hand::new(card1, card2)),
            (Err(err), _) => Err(HandParseError::Card1(String::from(s), err)),
            (_, Err(err)) => Err(HandParseError::Card2(String::from(s), err)),
        }
    }
}

#[derive(Debug, PartialEq)]
pub enum HandParseError {
    Card1(String, CardParseError),
    Card2(String, CardParseError),
}

impl fmt::Display for HandParseError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            HandParseError::Card1(ref s, ref cause) => write!(
                f,
                "Error parsing while parsing first Card in Hand - input: '{}', reason: {}",
                s, cause
            ),
            HandParseError::Card2(ref s, ref cause) => write!(
                f,
                "Error parsing while parsing second Card in Hand - input: '{}', reason: {}",
                s, cause
            )
        }
    }
}

impl error::Error for HandParseError {
    fn source(&self) -> Option<&(dyn error::Error + 'static)> {
        match *self {
            HandParseError::Card1(_, ref cause) => Some(cause),
            HandParseError::Card2(_, ref cause) => Some(cause),
        }
    }
}

#[cfg(test)]
mod tests {
    use core::model::card::card::Card;
    use core::model::card::rank::Rank;
    use core::model::card::suit::Suit;

    use super::*;

    #[test]
    fn should_parse_hand_with_8_of_heart_and_10_of_spade() {
        let result = WinamaxParser::parse("[8h Ts]");
        let expected = Hand::new(Card::new(Rank::Eight, Suit::Heart), Card::new(Rank::Ten, Suit::Spade));
        assert_eq!(Ok(expected), result)
    }

    #[test]
    fn should_return_error_when_failing_parsing_first_card_in_hand() {
        let input = "[8y Ts]";
        let result = <WinamaxParser as Parser<Hand>>::parse(input);
        match result {
            Err(HandParseError::Card1(ref s, _)) => assert_eq!(input, s),
            _ => assert!(false)
        }
    }

    #[test]
    fn should_return_error_when_failing_parsing_second_card_in_hand() {
        let input = "[8h 0s]";
        let result = <WinamaxParser as Parser<Hand>>::parse(input);
        match result {
            Err(HandParseError::Card2(ref s, _)) => assert_eq!(input, s),
            _ => assert!(false)
        }
    }
}

