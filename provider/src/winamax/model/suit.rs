use std::error;
use std::fmt;

use core::model::card::suit::Suit;

use crate::common::Parser;
use crate::winamax::parser::WinamaxParser;


impl Parser<Suit> for WinamaxParser {
    type Err = SuitParseError;

    fn parse(s: &str) -> Result<Suit, Self::Err> {
        match s {
            "c" => Ok(Suit::Club),
            "s" => Ok(Suit::Spade),
            "h" => Ok(Suit::Heart),
            "d" => Ok(Suit::Diamond),
            _ => Err(SuitParseError::UnknownSuit(String::from(s))),
        }
    }
}

#[derive(Debug, PartialEq)]
pub enum SuitParseError {
    UnknownSuit(String),
}

impl fmt::Display for SuitParseError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            SuitParseError::UnknownSuit(ref s) => {
                write!(f, "Error parsing Suit from string '{}'", s)
            }
        }
    }
}

impl error::Error for SuitParseError {
    fn source(&self) -> Option<&(dyn error::Error + 'static)> {
        None
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn should_parse_parse_club() {
        assert_eq!(Ok(Suit::Club), WinamaxParser::parse("c"));
    }

    #[test]
    fn should_parse_parse_spade() {
        assert_eq!(Ok(Suit::Spade), WinamaxParser::parse("s"));
    }

    #[test]
    fn should_parse_parse_heart() {
        assert_eq!(Ok(Suit::Heart), WinamaxParser::parse("h"));
    }

    #[test]
    fn should_parse_parse_diamond() {
        assert_eq!(Ok(Suit::Diamond), WinamaxParser::parse("d"));
    }

    #[test]
    fn should_return_error_when_parsing_unknown_suit() {
        let input = "unknown_rank";
        match <WinamaxParser as Parser<Suit>>::parse(input) {
            Err(SuitParseError::UnknownSuit(ref s)) => assert_eq!(input, s),
            _ => assert!(false),
        }
    }
}
