use std::error;
use std::fmt;

pub trait Parser<M> {
    type Err;
    fn parse(s: &str) -> Result<M, Self::Err>;
}

pub trait Enricher<M> {
    type Err;
    fn enrich(m: M, s: &str) -> Result<M, Self::Err>;
}
