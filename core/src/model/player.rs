use super::hand::Hand;

#[derive(Debug, Clone)]
pub struct Player {
    pub name: String,
    pub hand: Option<Hand>,
    pub stack: f32,
}

impl Player {
    pub fn new(name: String, hand: Option<Hand>,  stack: f32) -> Player {
        Player {name, hand, stack }
    }
}

impl PartialEq for Player {
    fn eq(&self, other: &Self) -> bool {
        self.name == other.name 
        && self.hand == other.hand
        && self.stack == self.stack 
    }
}
