use super::card::card::Card;

#[derive(PartialEq, Debug, Default, Clone)]
pub struct Board {
    pub flop: Option<Flop>,
    pub turn: Option<Turn>,
    pub river: Option<River>,
}

pub type Flop = (Card, Card, Card);
pub type Turn = Card;
pub type River = Card;
