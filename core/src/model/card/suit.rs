#[derive(Copy, Clone, PartialEq ,Debug)]
pub enum Suit {
    Club,
    Spade,
    Heart,
    Diamond,
}