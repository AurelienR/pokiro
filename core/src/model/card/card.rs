use super::rank::Rank;
use super::suit::Suit;

#[derive(Copy, Clone, Debug)]
pub struct Card {
    rank: Rank,
    suit: Suit,
}

impl Card {
    pub fn new(rank: Rank, suit: Suit) -> Self {
        Self {
            rank,
            suit
        }
    }
}

impl PartialEq for Card {
    fn eq(&self, other: &Self) -> bool {
        self.rank == other.rank
        && self.suit == other.suit
    }
}

impl Eq for Card {}