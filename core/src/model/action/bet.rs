use super::action::Action;

#[derive(PartialEq, Debug, Clone)]
pub struct Bet {
    pub player: String,
    pub amount: f32,
    pub all_in: bool,
}

impl Bet {
    pub fn new(player: String, amount: f32, all_in: bool) -> Self {
        Self {
            player,
            amount,
            all_in,
        }
    }
}

impl Default for Bet {
    fn default() -> Self {
        Bet {
            player: String::default(),
            amount: f32::default(),
            all_in: bool::default(),
        }
    }
}

impl Action for Bet{}
