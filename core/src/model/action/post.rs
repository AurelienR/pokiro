use super::action::Action;

#[derive(PartialEq, Debug, Clone)]
pub struct Post {
    pub player: String,
    pub kind: PostKind,
    pub amount: f32,
    // TODO: Check what happen for all-in cases?
}

impl Post {
    pub fn new(player: String, kind: PostKind, amount: f32) -> Self {
        Self {
            player,
            kind,
            amount,
        }
    }
}

impl Default for Post {
    fn default() -> Self {
        Post {
            player: String::default(),
            kind: PostKind::default(),
            amount: f32::default(),
        }
    }
}

#[derive(PartialEq, Debug, Clone)]
pub enum PostKind {
    Ante,
    SmallBlind,
    BigBlind,
}

impl Default for PostKind {
    fn default() -> Self {
        PostKind::Ante
    }
}

impl Action for Post {}

