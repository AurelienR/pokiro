use super::action::Action;

#[derive(PartialEq, Debug, Clone)]
pub struct Fold {
    pub player: String,
}

impl Fold {
    pub fn new(player: String) -> Self {
        Self{player}
    }
}

impl Default for Fold {
    fn default() -> Self {
        Fold {
            player: String::default(),
        }
    }
}

impl Action for Fold{}
