use crate::model::action::action::Action;

#[derive(PartialEq, Debug, Clone)]
pub struct PhaseStart {}

impl Action for PhaseStart {}

impl PhaseStart {
    pub fn new() -> Self { Self {} }
}
