use super::action::Action;

#[derive(PartialEq, Debug, Clone)]
pub struct Check {
    pub player: String,
}

impl Check {
    pub fn new(player: String) -> Self {
        Self {player}
    }
}

impl Default for Check {
    fn default() -> Self {
        Check {
            player: String::default(),
        }
    }
}

impl Action for Check {}
