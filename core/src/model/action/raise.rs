use super::action::Action;

#[derive(PartialEq, Debug, Clone)]
pub struct Raise {
    pub player: String,
    pub from: f32,
    pub to: f32,
    pub all_in: bool,
}

impl Raise {
    pub fn new(player: String, from: f32, to: f32, all_in: bool) -> Self {
        Self {
            player,
            from, 
            to,
            all_in,
        }
    }
}

impl Default for Raise {
    fn default() -> Self {
        Raise {
            player: String::default(),
            from: f32::default(),
            to: f32::default(),
            all_in: bool::default(),
        }
    }
}

impl Action for Raise{}
