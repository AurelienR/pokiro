use super::card::card::Card;

#[derive(PartialEq, Copy, Clone, Debug)]
pub struct Hand {
    card1: Card, 
    card2: Card
}

impl Hand {
    pub fn new(card1: Card, card2: Card) -> Self {
        Self{
            card1, 
            card2
        }
    }
}
