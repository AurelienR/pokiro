use chrono::prelude::{DateTime, Utc};
use super::player::Player;
use super::event::Event;
use super::board::Board;
use super::action::action::Action;

#[derive(Debug)]
pub struct Game{
    pub provider: String,
    pub provider_game_id: String,
    pub started_at: DateTime<Utc>,
    pub board: Board,
    pub seats: Vec<Option<Player>>,
    pub button_seat_index: usize,
    pub small_blind: f32,
    pub big_blind: f32,
    pub variant: String,
    pub game_type: String,
    pub game_details: String,
    pub table_name: String,
    pub money_type: String,
    pub player_max: usize,
    pub history: Vec<Event>,
}

impl Game {
    pub fn new() -> Self {
        Default::default()
    }
}

impl Default for Game {
    fn default() -> Self {
        Self {
            provider: String::from(""),
            provider_game_id: String::from(""),
            started_at: Utc::now(),
            variant: String::from(""),
            game_type: String::from(""),
            game_details: String::from(""),
            board: Board::default(),
            seats: Vec::default(),
            button_seat_index: usize::default(),
            small_blind: f32::default(),
            big_blind: f32::default(),
            table_name: String::from(""),
            money_type: String::from(""),
            player_max: usize::default(),
            history: Vec::default(),
        }
    }
}

impl PartialEq for Game {
    // TODO : Check if this is sufficient
    // For now cannot derive because history contains
    // heterogeneous collections that cannot impl PartialEq
    fn eq(&self, other: &Self) -> bool {
        self.provider == other.provider
        && self.provider_game_id == other.provider_game_id
    }
}
