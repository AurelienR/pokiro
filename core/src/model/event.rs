use super::phase::Phase;
use super::action::action::Action;

#[derive(Debug)]
pub struct Event {
    pub phase: Phase,
    pub action: Box<dyn Action>,
}

impl Event {
    pub fn new(phase: Phase, action: Box<dyn Action>) -> Self {
        Event{
            phase,
            action
        }
    }
}
