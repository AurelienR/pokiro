#[derive(PartialEq, Debug, Clone)]
pub enum Phase {
    AnteBlind,
    PreFlop,
    Flop,
    Turn,
    River,
}
